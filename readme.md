# Swift Deliveries (Backend) App

Swift Deliveries (Mobile and Web) App is an e-commerce platform for ordering the best foods, drinks and groceries in Lagos, Nigera.
This application is the backend system powering the mobile application as well as the web application

## Installation

### Clone the repository
```
git clone https://github.com/asdaviid/swift-deliveries-web.git
```

### Install the required dependencies
```
composer install
```

## Configuration
### Configure the environment file
Copy the content of the `.env.example` to `.env` if the `.env` does not exists yet and configure it based on application needs
```
cp .env.example .env
```

### Migrate database tables
```
php artisan migrate
```

### Generate credentials
In order to generate "personal access" and "password grant" clients which will be used to generate access tokens, do this:
```
php artisan passport:install
```

Or to regenerate the access keys use:

```
php artisan passport:install --force
```

For production, it is recommended by [Laravel](https://laravel.com/docs/5.6/passport#deploying-passport) to generate special passport keys for authentication

```
php artisan passport:keys
```

This will generate an access-token which should be appended to the header of each request made
