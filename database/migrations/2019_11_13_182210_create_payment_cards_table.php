<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('email');
            $table->string('access_code')->nullable();
            $table->string('authorization_code')->nullable();
            $table->string('card_type')->nullable();
            $table->string('last_four_digits', 10)->nullable();
            $table->string('expiry_month', 10)->nullable();
            $table->string('expiry_year', 10)->nullable();
            $table->boolean('is_reusable')->default(true);
            $table->timestamp('active_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('payment_cards', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_cards');
    }
}
