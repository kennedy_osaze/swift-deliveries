<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoordinatesToBillingContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_contacts', function (Blueprint $table) {
            $table->dropColumn(['city', 'state']);

            $table->decimal('latitude', 10, 8)->after('address')->nullable()->index();
            $table->decimal('longitude', 11, 8)->after('latitude')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_contacts', function (Blueprint $table) {
            $table->string('state')->nullable();
            $table->string('city')->nullable();

            $table->dropIndex(['latitude', 'longitude']);
            $table->dropColumn(['latitude', 'longitude']);
        });
    }
}
