<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnsInOrdersAndPaymentCardsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->foreign('cart_id')->references('id')->on('carts');
        });

        Schema::table('payment_cards', function (Blueprint $table) {
            $table->dropColumn('access_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_cards', function (Blueprint $table) {
            $table->string('access_code')->after('email')->nullable();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['cart_id']);
        });
    }
}
