<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use App\Models\UserRating;
use Faker\Generator as Faker;

$factory->define(UserRating::class, function (Faker $faker) {
    return [
        'comment' => $faker->sentence(),
        'rating' => rand(1, 5),
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});
