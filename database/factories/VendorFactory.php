<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Vendor;
use Illuminate\Support\Arr;
use App\Models\VendorOutlet;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    $random_suffix = Arr::random([
        'Eatery', 'Joint', 'Restaurant', 'Outlet', 'Mall',
        'Cafeteria', 'Kitchen',
    ]);

    return [
        'name' => ucwords($faker->words(2, true)) . ' ' . $random_suffix,
        'description' => $faker->text(),
        'vendor_type' => Arr::random(Vendor::TYPES),
        'logo' => '/',
        'active_at' => now(),
    ];
});

$factory->state(Vendor::class, 'with_random_outlets', []);

$factory->afterCreatingState(
    Vendor::class,
    'with_random_outlets',
    function ($vendor, $faker) {
        $vendor->outlets()->createMany(
            factory(VendorOutlet::class, rand(1, 5))->make()->toArray()
        );
    }
);
