<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OutletProduct;
use Faker\Generator as Faker;

$factory->define(OutletProduct::class, function (Faker $faker) {
    return [
        'price' => $faker->randomFloat(2, 100, 5000),
        'active_at' => now(),
    ];
});
