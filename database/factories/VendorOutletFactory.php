<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Arr;
use App\Models\VendorOutlet;
use Faker\Generator as Faker;

$factory->define(VendorOutlet::class, function (Faker $faker) {
    // Latitude and longitude coordinates of Lagos are used here.
    $cities = [
        'Ketu', 'Ikoyi', 'Victoria Island', 'Ijora', 'Mile 12', 'Mile2',
        'Ajah', 'Ikorodu', 'Ojota', 'MaryLand', 'Shomolu', 'CMS',
        'Obalende', 'Ebute-Meta', 'Lekki', 'Ikeja',
    ];

    return [
        'latitude' => $faker->latitude(6.4, 6.6),
        'longitude' => $faker->longitude(2.8, 3.6),
        'address' => $faker->streetAddress,
        'city' => Arr::random($cities),
        'state' => 'Lagos',
        'active_at' => now(),
    ];
});
