<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;
use App\Models\ProductCategory;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'=> ucwords($faker->words('2', true)),
        'description' => $faker->sentence(rand(6, 20)),
        'image' => '/',
        'active_at' => now(),
    ];
});

$factory->state(Product::class, 'with_category', [
    'category_id' => function () {
        return factory(ProductCategory::class)->create()->id;
    }
]);
