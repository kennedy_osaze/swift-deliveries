<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use App\Models\UserFavourites;

$factory->define(UserFavourites::class, function (Faker $faker) {
    $now = now();

    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});
