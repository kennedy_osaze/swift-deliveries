<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Arr;
use Faker\Generator as Faker;
use App\Models\ProductCategory;

$factory->define(ProductCategory::class, function (Faker $faker) {
    $category_type = Arr::random(ProductCategory::PRODUCT_TYPES);

    return [
        'name' => ucwords($faker->word . ' ' . $category_type),
        'product_type' => $category_type,
        'image' => '/',
        'active_at' => now(),
    ];
});

$factory->state(ProductCategory::class, 'food', function ($faker) {
    return [
        'name' => ucwords("{$faker->word} Food"),
        'product_type' => ProductCategory::PRODUCT_TYPES['FOOD'],
    ];
});

$factory->state(ProductCategory::class, 'drink', function ($faker) {
    return [
        'name' => ucwords("{$faker->word} Drink"),
        'product_type' => ProductCategory::PRODUCT_TYPES['DRINK'],
    ];
});
