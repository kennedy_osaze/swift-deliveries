<?php

use App\Models\User;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\UserRating;
use App\Models\OutletProduct;
use App\Models\UserFavourites;
use Illuminate\Database\Seeder;

class UserFavouritesAndRatingsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = factory(Product::class, 3)
            ->states('with_category')
            ->create();

        $vendors = factory(Vendor::class, 3)
            ->states('with_random_outlets')
            ->create(['logo' => null])
            ->loadMissing('outlets');

        $vendors->each(function ($vendor) use ($products) {
            $vendor->productImages()->createMany(
                $products->map(function ($product) {
                    return ['product_id' => $product->id, 'image' => '/'];
                })->toArray()
            );
        });

        $products->shuffle()->each(function ($product) use ($vendors) {
            $vendors->shuffle()->each(function ($vendor) use ($product) {
                $vendor->outlets->shuffle()->each(function ($outlet) use ($product) {
                    $outlet_product = factory(OutletProduct::class)->create([
                        'outlet_id' => $outlet->id,
                        'product_id' => $product->id,
                    ]);

                    $outlet_product->favourites()->save(
                        factory(UserFavourites::class)->make()
                    );

                    $outlet_product->ratings()->save(
                        factory(UserRating::class)->make()
                    );

                    $user = User::first();
                    $user->favouredOutletProducts()->attach($outlet_product->id);
                    $user->ratedOutletProducts()->attach($outlet_product->id, [
                        'rating' => rand(1, 5),
                    ]);
                });
            });
        });
    }
}
