<?php

use App\Models\Vendor;
use App\Models\Product;
use App\Models\OutletProduct;
use Illuminate\Support\Arr;
use App\Models\ProductCategory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Collection;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = $this->generateProductCategories();
        $vendors = $this->generateVendorsWithOutlets();
        $products = $this->generateProducts($categories);

        $vendors = $vendors->loadMissing('outlets');

        $products->shuffle()->each(function ($product) use ($vendors) {
            $vendors->shuffle()->each(function ($vendor) use ($product) {
                foreach (range(1, 2) as $value) {
                    $product->vendorImages()->create([
                        'vendor_id' => $vendor->id,
                        'image' => '/',
                    ]);
                }
            });

            $outlets = $vendors->pluck('outlets')->flatten();
            $now = now();

            $outlets->shuffle()->each(function ($outlet) use ($product, $now) {
                factory(OutletProduct::class)->create([
                    'outlet_id' => $outlet->id,
                    'product_id' => $product->id,
                    'active_at' => Arr::random([$now, null, $now, $now]),
                ]);
            });
        });
    }

    /**
     * Generate Product Categories
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function generateProductCategories()
    {
        $categories = Collection::make([]);

        $now = now();
        foreach (range(1, 10) as $value) {
            $category = factory(ProductCategory::class)->create([
                'active_at' => Arr::random([$now, null, $now, $now]),
            ]);

            $categories->push($category);
        }

        return $categories;
    }

    /**
     * Generate Vendors with their corresponding outlets
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function generateVendorsWithOutlets()
    {
        $vendors = Collection::make([]);

        $now = now();
        foreach (range(1, 10) as $value) {
            $vendor = factory(Vendor::class)->states('with_random_outlets')->create([
                'active_at' => Arr::random([$now, null, $now, $now]),
            ]);

            $vendors->push($vendor);
        }

        return $vendors;
    }

    /**
     * Generate Products based on Product Categories
     *
     * @param \Illuminate\Database\Eloquent\Collection $categories
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function generateProducts($categories)
    {
        $products = collect([]);
        $now = now();

        $categories->shuffle()->each(function ($category) use ($now, $products) {
            $_products = $category->products()->createMany(
                factory(Product::class, rand(1, 5))->make([
                    'active_at' => Arr::random([$now, null, $now, $now]),
                ])->toArray()
            );

            $products->push($_products);
        });

        return Collection::make($products->collapse()->all());
    }
}
