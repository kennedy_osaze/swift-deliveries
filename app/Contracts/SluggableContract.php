<?php

namespace App\Contracts;

interface SluggableContract
{
    /**
     * Get configuration array.
     *
     * @return array
     */
    public function sluggable();

    /**
     * The field name to slug
     *
     * @return string
     */
    public function sluggableKeyName();
}
