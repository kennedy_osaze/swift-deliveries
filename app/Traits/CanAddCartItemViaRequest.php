<?php

namespace App\Traits;

use App\Models\OutletProduct;
use Illuminate\Support\Facades\Auth;

trait CanAddCartItemViaRequest
{
    /**
     * Check if the item has been added to the user cart already
     *
     * @param string $item
     *
     * @return bool
     */
    private function doesCartItemExistsInUserCart(string $item)
    {
        return Auth::user()->activeCart()
            ->whereHas('items', function ($query) use ($item) {
                $query->where('item_id', hash_id_decode($item));
            })
            ->exists();
    }

    /**
     * Check if the item is available for sale
     *
     * @param string $item
     *
     * @return bool
     */
    private function isItemAvailable(string $item)
    {
        return OutletProduct::whereHashId($item)->available()->exists();
    }

    /**
     * Checks if an item is of the same outlet as the ones already added
     *
     * @param string $item
     *
     * @return bool
     */
    private function itemIsOfSameOutlet(string $item)
    {
        $cart = Auth::user()->activeCart;

        if (! $cart || $cart->items()->doesntExist()) {
            return true;
        }

        return $cart->canAddItemWithOutletAsOtherItemsAdded($item);
    }
}
