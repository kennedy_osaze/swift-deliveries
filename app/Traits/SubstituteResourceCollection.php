<?php

namespace App\Traits;

use App\Http\Resources\AnonymousResourceCollection;

trait SubstituteResourceCollection {
    /**
     * Create new anonymous resource collection.
     *
     * @param mixed  $resource
     *
     * @return \App\Http\Resources\AnonymousResourceCollection
     */
    public static function collection($resource)
    {
        return tap(
            new AnonymousResourceCollection($resource, static::class, static::metaData()),
            function ($collection) {
                if (property_exists(static::class, 'preserveKeys')) {
                    $collection->preserveKeys = (new static([]))->preserveKeys === true;
                }
            }
        );
    }

    /**
     * Gets the meta data for the anonymous resource collection
     *
     * @return array
     */
    protected static function metaData()
    {
        return [
            'response_message' => '',
        ];
    }
}
