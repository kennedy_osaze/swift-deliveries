<?php

namespace App\Traits;

use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    /**
     * This scopes the builder based on the selected filter class
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param \App\Filters\BaseFilter
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function scopeFilter(Builder $builder, BaseFilter $filter)
    {
        return $filter->apply($builder);
    }
}
