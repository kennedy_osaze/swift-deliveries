<?php

namespace App\Traits;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use App\Exceptions\HttpExceptionWithErrorData;
use App\Exceptions\ValidationResponseException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait HandleApiExceptions {
    /**
     * The filename containing API error language strings
     *
     * @var string
     */
    protected $api_error_translation_file = 'api_error';

    /**
     * Transforms an exception into a JSON response
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getJsonResponseForException($request, Exception $exception)
    {
        $exception = $this->prepareApiException($exception, $request);

        if ($exception instanceof HttpResponseException) {
            return $exception->getResponse();
        }

        if ($exception instanceof AuthenticationException) {
            $exception = new HttpException(401, $exception->getMessage(), $exception);
        }

        $exception_response_data = $this->jsonExceptionData($exception);

        $status_code = $exception_response_data['status_code'];
        unset($exception_response_data['status_code']);

        $headers = $this->isHttpException($exception) ? $exception->getHeaders() : [];

        return response()->json($exception_response_data, $status_code, $headers);
    }

    /**
     * Prepare exception for  API rendering.
     *
     * @param  \Exception  $e
     *
     * @return \Exception
     */
    protected function prepareApiException($exception, $request)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof NotFoundHttpException) {
            $exception = new HttpException(404, 'Resource not found.', $exception);
        } elseif ($exception instanceof ValidationException) {
            $exception = new ValidationResponseException($exception->validator, $request);
        }

        return $exception;
    }

    /**
     * Convert exception to array equivalent
     *
     * @param  \Exception  $exception
     *
     * @return array
     */
    protected function jsonExceptionData(Exception $exception)
    {
        $status_code = $this->isHttpException($exception)
            ? $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;

        $error_data = $this->getTranslationDataFromException($exception, $status_code);
        $response_data = array_merge([
            'status_code' => $status_code,
            'status' => false,
            'message' => $error_data['message'],
        ]);

        if ($exception instanceof HttpExceptionWithErrorData) {
            $response_data['error'] = $exception->getError();
        } else if (($error = collect($error_data)->except(['message']))->isNotEmpty()) {
            $response_data['error'] = $error->all();
        }

        if (config('app.debug') && $status_code >= 500) {
            $response_data['error']['debug'] = $this->convertExceptionToArray($exception);
        }

        return $response_data;
    }

    /**
     * Returns the message and error code for the exception based on localization keys defined
     *
     * @param  \Exception  $exception
     * @param int $status
     *
     * @return array
     */
    protected function getTranslationDataFromException(Exception $exception, $status)
    {
        $translation_data = [];

        $error_message = $exception->getMessage() ?: Response::$statusTexts[$status];

        $translation_key = "{$this->api_error_translation_file}.{$error_message}";
        $translated_message = __($translation_key);

        $translation_key_used = ! Str::startsWith($translated_message, $this->api_error_translation_file);
        $translation_data['message'] = $translation_key_used ? $translated_message : $error_message;

        if ($translation_key_used) {
            $translation_data['code'] = Str::slug($error_message, '_');
        }

        return $translation_data;
    }
}
