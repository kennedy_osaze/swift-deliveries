<?php 

namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadImageTrait
{
    public function uploadImage(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }

    /**
     * Upload image to folder
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Support\Str
     */
    public function processImageRequest(Request $request, $path)
    {
        // Get image file
        $image = $request->file('image');
   
        if($request->name)
        {
            // Make a image name based on user name and current timestamp
            $name = Str::slug($request->input('name')).'_'.time();
        }else{
            $name = 'img'.'_'.time();
        }
        
        // Define folder path
        $folder = $path;
        // Make a file path where image will be stored [ folder path + file name + file extension]
        $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        // Upload image
        $this->uploadImage($image, $folder, 'public', $name);
        // Set user profile image path in database to filePath
        return $filePath;
    }

    /**
     * Upload multiple image to folder
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Support\Arr
     */
    public function processMultipleImageRequest(Request $request, $path)
    {
        // Get image file
        $image = $request->file('image');

        $url_paths = [];
        
        foreach($image as $img)
        {
            if($request->name)
            {
                // Make a image name based on user name and current timestamp
                $name = Str::slug($request->input('name')).'_'.time();
            }else{
                $name = 'img'.'_'.time();
            }

            // Define folder path
            $folder = $path;
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $img->getClientOriginalExtension();
            // Upload image
            $this->uploadImage($img, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            array_push($url_paths, $filePath);
        }

        return $url_paths;
    }
}