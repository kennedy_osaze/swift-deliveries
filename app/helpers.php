<?php

use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;
use App\Exceptions\HttpExceptionWithErrorData;

if (! function_exists('hash_id_encode')) {
    /**
     * Encodes the key to an hashId
     *
     * @param mixed $key
     * @param string|null $connection
     *
     * @return array|string
     */
    function hash_id_encode($key, string $connection = null) {
        return Hashids::connection(
            $connection ?: Hashids::getDefaultConnection()
        )->encode($key);
    }
}

if (! function_exists('hash_id_decode')) {
    /**
     * Decode the hashid to its original value
     *
     * @param string $hash
     * @param string|null $connection
     *
     * @return mixed
     */
    function hash_id_decode(string $hash, string $connection = null) {
        $decoded = Hashids::connection(
            $connection ?: Hashids::getDefaultConnection()
        )->decode($hash);

        return count($decoded) > 1 ? $decoded : head($decoded);
    }
}

if (! function_exists('abort_with_error')) {
    /**
     * Throw an HttpExceptionWithErrorData with the given error data.
     *
     * @param int $code
     * @param string $message
     * @param array $error
     * @param array $headers
     *
     * @throws \App\Exceptions\HttpExceptionWithErrorData
     */
    function abort_with_error(int $code, array $error, string $message = '', array $headers = [])
    {
        throw new HttpExceptionWithErrorData($code, $error, $message, $headers);
    }
}

if (! function_exists('queryOfTableWithDistanceInKmBasedOnCoordinates')) {
    /**
     * Returns a query that can return all records including a computed distance,
     * in kilometers as a column, between a certain coordinate (longitude, latitude)
     * and those of the table defined coordinates
     *
     * @param string $table The table to query
     * @param float $longitude The longitude to calculate distance from
     * @param float $latitude The latitude to calculate distance from
     * @param array $select The list of columns to retrieve
     * @param string The name of the table longitude column
     * @param string The name of the table latitude column
     *
     * @return \Illuminate\Database\Query\Builder
     */
    function queryOfTableWithDistanceInKmBasedOnCoordinates(
        string $table,
        float $longitude,
        float $latitude,
        array $select = ['*'],
        string $longitude_column = 'longitude',
        string $latitude_column = 'latitude'
    ) {
        $calculate_distance_query = <<<QUERY
        (6371 * acos(
            cos(radians(?))
            * cos(radians($table.$latitude_column))
            * cos(radians($table.$longitude_column) - radians(?))
            + sin(radians(?))
            * sin(radians($table.$latitude_column))
        )) as distance
        QUERY;

        return DB::table($table)
            ->select($select)
            ->selectRaw($calculate_distance_query, [
                $latitude, $longitude, $latitude
            ]);
    }
}
