<?php

namespace App\Builders;

use App\Models\User;
use App\Models\Vendor;
use App\Models\ProductCategory;

class ProductBuilder extends BaseBuilder
{
    public function active()
    {
        parent::activeForTable('products');

        return $this;
    }

    public function hasActiveCategory()
    {
        $this->where(function (ProductBuilder $query) {
            $query->whereHas('category', function ($query) {
                $query->active();
            });
        });

        return $this;
    }

    public function activeAsCategory()
    {
        $this->where(function (ProductBuilder $query) {
            $query->active()->hasActiveCategory();
        });

        return $this;
    }

    public function hasAvailableItems()
    {
        $this->where(function ($query) {
            $query->whereHas('outletProductPivot', function ($query) {
                $query->hasAvailableItemsViaVendors();
            });
        });

        return $this;
    }

    public function available()
    {
        $this->where(function (ProductBuilder $query) {
            $query->activeAsCategory()->hasAvailableItems();
        });

        return $this;
    }

    public function availableAsCategory(ProductCategory $category)
    {
        $this->where(function (ProductBuilder $query) use ($category) {
            $query->available()->where('category_id', $category->id);
        });

        return $this;
    }

    public function availableAsVendor(Vendor $vendor)
    {
        $this->activeAsCategory()->hasAvailableVendors($vendor);

        return $this;
    }

    public function hasAvailableVendors(Vendor $vendor = null)
    {
        $this->where(function (ProductBuilder $query) use ($vendor) {
            $query->whereHas(
                'outletProductPivot',
                function (OutletProductBuilder $query) use ($vendor) {
                    $query->hasAvailableItemsViaVendors()
                        ->when($vendor, function ($query, $vendor) {
                            return $query->whereVendor($vendor);
                        });
                }
            );
        });

        return $this;
    }

    public function withItemsDetails(Vendor $vendor = null)
    {
        $this->with([
            'category',
            'outletProductsPivot' => function ($query) use ($vendor) {
                $query->available()
                    ->when($vendor, function ($query, $vendor) {
                        return $query->whereVendor($vendor);
                    })
                    ->with('outlet.vendor');
            }
        ]);

        return $this;
    }

    public function withAnyVendorItemImageAvailable(Vendor $vendor = null)
    {
        $this->with([
            'randomVendorProductImage' => function ($query) use ($vendor) {
                $query->whereHas('vendor', function ($query) use ($vendor) {
                    $query->active()
                        ->when($vendor, function ($query, $vendor) {
                            return $query->where('vendors.id', $vendor->id);
                        });
                });
            }
        ]);

        return $this;
    }

    // public function

    public function withRatingsCount(Vendor $vendor = null) {
        $this->withCount([
            'ratings' => function ($query) use ($vendor) {
                $query->when($vendor, function ($query, $vendor) {
                    return $query->hasItemVendor($vendor);
                });
            }
        ]);

        return $this;
    }

    public function withAverageRatings(Vendor $vendor = null)
    {
        $this->with([
            'ratingsAverage' => function ($query) use ($vendor) {
                $query->when($vendor, function ($query, $vendor) {
                    return $query->hasItemVendor($vendor);
                });
            }
        ]);

        return $this;
    }

    public function withRatingsForVendors(Vendor $vendor = null)
    {
        $this->withRatingsCount($vendor)
            ->withAverageRatings($vendor);

        return $this;
    }

    public function withAvailableFavouritesCount()
    {
        $this->withCount(['favourites' => function (FavouritesBuilder $query) {
            $query->hasAvailableItems();
        }]);

        return $this;
    }

    public function withUserFavourites(
        User $user = null,
        Vendor $vendor = null
    ) {
        $this->withCount([
            'favourites' => function ($query) use ($user, $vendor) {
                $query
                    ->when($user, function ($query, $user) {
                        return $query->where('user_id', $user->id);
                    })
                    ->when($vendor, function ($query, $vendor) {
                        return $query->hasItemVendor($vendor);
                    });
            }
        ]);

        return $this;
    }

    public function withCountAvailableVendors()
    {
        $this->selectColumn();

        $vendors_count_query = Vendor::selectRaw('count(distinct vendors.name)')
            ->join('vendor_outlets', 'vendor_outlets.vendor_id', '=', 'vendors.id')
            ->join('outlet_product', 'outlet_product.outlet_id', '=', 'vendor_outlets.id')

            ->whereColumn('products.id', 'outlet_product.product_id')
            ->whereNotNull('outlet_product.active_at')
            ->whereNotNull('vendor_outlets.active_at')
            ->whereNotNull('vendors.active_at')
            ->whereNull('vendor_outlets.deleted_at')
            ->whereNull('outlet_product.deleted_at');

        $this->selectSub($vendors_count_query, 'available_vendors_count');

        return $this;
    }

    public function withAvailableOutletsAndItemsCounts(Vendor $vendor = null)
    {
        $this->withCount([
            'outletProductPivot' => function ($query) use ($vendor) {
                $query->available()
                    ->when($vendor, function ($query, $vendor) {
                        return $query->whereHas(
                            'outlet',
                            function ($query) use ($vendor) {
                                $query->where('vendor_id', $vendor->id);
                            }
                        );
                    });
            },
            'outlets' => function ($query) use ($vendor) {
                $query->available()
                    ->when($vendor, function ($query, $vendor) {
                        return $query->where('vendor_id', $vendor->id);
                    });
            }
        ]);

        return $this;
    }

    public function withProductDetails()
    {
        $this->with('category')
            ->withRatingsCount()
            ->withAverageRatings()
            ->withAnyVendorItemImageAvailable()
            ->withCountAvailableVendors()
            ->withAvailableOutletsAndItemsCounts();

        return $this;
    }
}
