<?php

namespace App\Builders;

use App\Models\Vendor;

class ProductCategoryBuilder extends BaseBuilder
{
    public function active()
    {
        parent::activeForTable('product_categories');

        return $this;
    }

    public function hasActiveProducts()
    {
        $this->where(function (ProductCategoryBuilder $query) {
            $query->whereHas('products', function ($query) {
                $query->active();
            });
        });

        return $this;
    }

    public function activeAsProducts()
    {
        $this->where(function (ProductCategoryBuilder $query) {
            return $query->active()->hasActiveProducts();
        });

        return $this;
    }

    public function hasAvailableProducts()
    {
        $this->where(function (ProductCategoryBuilder $query) {
            $query->whereHas('products', function ($query) {
                $query->available();
            });
        });

        return $this;
    }

    public function hasAvailableItems()
    {
        $this->where(function (ProductCategoryBuilder $query) {
            $query->whereHas('outletProducts', function ($query) {
                $query->available();
            });
        });

        return $this;
    }

    public function available()
    {
        $this->where(function (ProductCategoryBuilder $query) {
            $query->active()->hasAvailableItems();
        });

        return $this;
    }

    public function hasAvailableVendor(Vendor $vendor)
    {
        $this->where(function (ProductCategoryBuilder $query) use ($vendor) {
            $query->whereHas('outletProducts', function ($query) use ($vendor) {
                $query->available()->whereVendor($vendor);
            });
        });

        return $this;
    }

    public function availableAsVendor(Vendor $vendor)
    {
        $this->activeAsProducts()->hasAvailableVendor($vendor);

        return $this;
    }

    public function withCountOfAvailableItems(Vendor $vendor = null)
    {
        $this->withCount([
            'outletProducts as available_items' =>
            function (OutletProductBuilder $query) use ($vendor) {
                $query->available()
                    ->when($vendor, function ($query) {
                        return $query;
                    });
            }
        ]);

        return $this;
    }

    public function withAvailableVendors()
    {
        $this->with(['customRelationVendors' => function ($query) {
            $query->available()
                ->whereNull('vendor_outlets.deleted_at')
                ->whereNotNull('vendor_outlets.active_at')
                ->whereNull('vendors.deleted_at')
                ->whereNotNull('vendors.active_at');
        }]);

        return $this;
    }

    public function withCountAvailableVendors()
    {
        $this->selectColumn();

        $vendors_count_query = Vendor::selectRaw('count(distinct vendors.name)')
            ->join('vendor_outlets', 'vendor_outlets.vendor_id', '=', 'vendors.id')
            ->join('outlet_product', 'outlet_product.outlet_id', '=', 'vendor_outlets.id')
            ->join('products', 'products.id', '=', 'outlet_product.product_id')

            ->whereColumn('product_categories.id', 'products.category_id')
            ->whereNotNull('outlet_product.active_at')
            ->whereNotNull('products.active_at')
            ->whereNotNull('vendor_outlets.active_at')
            ->whereNotNull('vendors.active_at')
            ->whereNull('vendor_outlets.deleted_at')
            ->whereNull('outlet_product.deleted_at')
            ->whereNull('products.deleted_at');

        $this->selectSub($vendors_count_query, 'available_vendors_count');

        return $this;
    }

    public function withProductDetails()
    {
        $this->with(['products' => function ($query) {
            $query->withCount('ratings')
                ->with('ratingsAverage')
                ->withAnyVendorItemImageAvailable();
        }]);

        return $this;
    }
}
