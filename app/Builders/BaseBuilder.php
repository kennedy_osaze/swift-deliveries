<?php

namespace App\Builders;

use App\Filters\BaseFilter;
use Vinkla\Hashids\Facades\Hashids;
use App\Contracts\SluggableContract;
use Illuminate\Database\Eloquent\Builder;

class BaseBuilder extends Builder
{
    public function activeForTable(string $table)
    {
        $this->whereNotNull("{$table}.active_at");

        return $this;
    }

    public function whereHashId(string $hash_id)
    {
        $model = $this->getModel();
        $model_key_name = $model->getTable() . '.' . $model->getKeyName();
        $this->where($model_key_name, $this->hashIdToId($model, $hash_id));

        return $this;
    }

    public function whereSlug(string $slug, string $key_name = '')
    {
        $model = $this->getModel();
        $model_is_sluggable = $model instanceof SluggableContract;

        if (! $model_is_sluggable && $key_name === '') {
            return $this;
        }

        $column = ($model_is_sluggable) ? $model->sluggableKeyName() : $key_name;
        if ($column !== '') {
            $this->where($model->getTable() . '.' . $column, $slug);
        }

        return $this;
    }

    public function whereHashIdOrSlug($identity)
    {
        $this->where(function (BaseBuilder $query) use ($identity) {
            $query->whereHashId($identity)->orWhere->whereSlug($identity);
        });

        return $this;
    }

    protected function hashIdToId($model, $hash_id)
    {
        if (! method_exists($model, 'getHashidsConnection')) {
            return '';
        }

        $decoded_ids = Hashids::connection(
            $model->getHashidsConnection()
        )->decode($hash_id);

        return ! empty($decoded_ids) ? $decoded_ids[0] : '';
    }

    public function selectColumn(string $column = '*')
    {
        if (is_null($this->query->columns)) {
            $this->query->select([$this->query->from . ".{$column}"]);
        }
    }

    public function filter(BaseFilter $filter)
    {
        return $filter->apply($this);
    }
}
