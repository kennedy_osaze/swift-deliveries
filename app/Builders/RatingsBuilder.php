<?php

namespace App\Builders;

use App\Models\Vendor;
use App\Models\OutletProduct;
use App\Models\ProductCategory;

class RatingsBuilder extends BaseBuilder
{
    public function hasItemCategory(ProductCategory $category)
    {
        $this->whereHasMorph(
            'ratable',
            OutletProduct::class,
            function ($query) use ($category) {
                $query->whereHas('product', function ($query) use ($category) {
                    return $query->where('products.category_id', $category->id);
                });
            }
        );

        return $this;
    }

    public function hasItemVendor(Vendor $vendor)
    {
        $this->whereHasMorph(
            'ratable',
            OutletProduct::class,
            function ($query) use ($vendor) {
                $query->whereHas('outlet', function ($query) use ($vendor) {
                    return $query->where('vendor_outlets.vendor_id', $vendor->id);
                });
            }
        );
    }
}
