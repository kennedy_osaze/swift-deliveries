<?php

namespace App\Builders;

use App\Models\ProductCategory;

class VendorOutletBuilder extends BaseBuilder
{
    public function active()
    {
        parent::activeForTable('vendor_outlets');

        return $this;
    }

    public function hasActiveVendor()
    {
        $this->where(function (VendorOutletBuilder $query) {
            $query->whereHas('vendor', function (VendorBuilder $query) {
                $query->active();
            });
        });

        return $this;
    }

    public function activeAsVendor()
    {
        $this->where(function (VendorOutletBuilder $query) {
            $query->active()->hasActiveVendor();
        });

        return $this;
    }

    public function hasAvailableItems()
    {
        $this->where(function ($query) {
            $query->whereHas('outletProductPivot', function ($query) {
                $query->hasAvailableItemsViaProducts();
            });
        });

        return $this;
    }

    public function available()
    {
        $this->where(function (VendorOutletBuilder $query) {
            $query->activeAsVendor()->hasAvailableItems();
        });

        return $this;
    }

    public function withRatingCount(ProductCategory $category = null)
    {
        $this->withCount([
            'ratings' => function ($query) use ($category) {
                $query->when($category, function ($query, $category) {
                    $query->hasItemCategory($category);
                });
            }
        ]);

        return $this;
    }

    public function withRatingsAverages(ProductCategory $category = null)
    {
        $this->with(['ratingsAverage' => function ($query) use ($category) {
            $query->when($category, function ($query, $category) {
                $query->hasItemCategory($category);
            });
        }]);

        return $this;
    }
}
