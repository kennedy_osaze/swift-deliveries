<?php

namespace App\Builders;

use App\Models\Product;
use App\Models\ProductCategory;

class VendorBuilder extends BaseBuilder
{
    public function active()
    {
        parent::activeForTable('vendors');

        return $this;
    }

    public function hasActiveOutlets()
    {
        $this->where(function (VendorBuilder $query) {
            $query->whereHas('outlets', function (VendorOutletBuilder $query) {
                $query->active();
            });
        });

        return $this;
    }

    public function activeAsOutlets()
    {
        $this->where(function (VendorBuilder $query) {
            $query->active()->hasActiveOutlets();
        });

        return $this;
    }

    public function hasAvailableOutlets()
    {
        $this->where(function (VendorBuilder $query) {
            $query->whereHas('outlets', function (VendorOutletBuilder $query) {
                $query->available();
            });
        });

        return $this;
    }

    public function available()
    {
        $this->where(function (VendorBuilder $query) {
            $query->active()->hasAvailableItems();
        });

        return $this;
    }

    public function hasAvailableItems()
    {
        $this->where(function ($query) {
            $query->whereHas(
                'outletProducts',
                function (OutletProductBuilder $query) {
                    $query->available();
                }
            );
        });

        return $this;
    }

    public function hasAvailableCategory(ProductCategory $category)
    {
        $this->where(function ($query) use ($category) {
            $query->whereHas('outletProducts', function ($query) use ($category) {
                $query->available()->whereProductCategory($category);
            });
        });

        return $this;
    }

    public function availableAsCategory(ProductCategory $category)
    {
        $this->where(function (VendorBuilder $query) use ($category) {
            $query->activeAsOutlets()->hasAvailableCategory($category);
        });

        return $this;
    }

    public function whereHasProductIsAvailable(Product $product)
    {
        $this->where(function (VendorBuilder $query) use ($product) {
            $query->available()
                ->whereHas(
                    'outlets.products',
                    function ($query) use ($product) {
                        $query->where('products.id', $product->id);
                    }
                );
        });

        return $this;
    }

    public function withCountOfAvailableItems(
        ProductCategory $category = null
    ) {
        $this->withCount([
            'outletProducts as available_items_count' =>
            function (OutletProductBuilder $query) use ($category) {
                $query->available()
                    ->when($category, function ($query, $category) {
                        return $query->whereProductCategory($category);
                    });
            }
        ]);

        return $this;
    }

    public function withCountOfAvailableProductItems(
        Product $product
    ) {
        $this->withCount([
            'outletProducts as available_items_count' =>
            function (OutletProductBuilder $query) use ($product) {
                $query->available()
                    ->when($product, function ($query, $product) {
                        return $query->where('product_id', $product->id);
                    });
            }
        ]);

        return $this;
    }

    public function withAnyVendorItemImageAvailable(
        ProductCategory $category = null
    ) {
        $this->with([
            'randomProductImage' => function ($query) use ($category) {
                $query->whereHas('product', function ($query) use ($category) {
                    $query->when($category, function ($query, $category) {
                        return $query->where('category_id', $category->id);
                    })->active();
                });
            }]);

        return $this;
    }

    public function withAnyAvailableVendorItemImageForProduct(
        Product $product
    ) {
        $this->with([
            'randomProductImage' => function ($query) use ($product) {
                $query->whereHas('product', function ($query) use ($product) {
                    $query->where('products.id', $product->id);
                });
            }
        ]);

        return $this;
    }

    public function withOutletDetails(
        ProductCategory $category = null
    ) {
        $this->withAnyVendorItemImageAvailable($category)
            ->with(['outlets' => function ($query) use ($category) {
                $query->active()
                    ->withRatingCount($category)
                    ->withRatingsAverages($category);
            }]);

        return $this;
    }

    public function withOutletsDetailsForProduct(
        Product $product
    ) {
        $this->withAnyAvailableVendorItemImageForProduct($product)
            ->with(['outlets' => function ($query) use ($product) {
                $query->whereHas(
                    'products',
                    function ($query) use ($product) {
                        $query->where('products.id', $product->id);
                    }
                )
                ->withRatingCount()
                ->withRatingsAverages();
            }]);

        return $this;
    }

    public function withAvailableCategories()
    {
        $this->with(['customRelationProductCategories' => function ($query) {
            $query->available()
                ->whereNull('products.deleted_at')
                ->whereNotNull('products.active_at')
                ->whereNull('product_categories.deleted_at')
                ->whereNotNull('product_categories.active_at');
        }]);

        return $this;
    }

    public function withCountAvailableCategories()
    {
        $this->selectColumn();

        $categories_count_query =
            ProductCategory::selectRaw('count(distinct product_categories.name)')
                ->join('products', 'products.category_id', '=', 'product_categories.id')
                ->join('outlet_product', 'outlet_product.product_id', '=', 'products.id')
                ->join('vendor_outlets', 'vendor_outlets.id', '=', 'outlet_product.outlet_id')

                ->whereColumn('vendors.id', 'vendor_outlets.vendor_id')
                ->whereNotNull('outlet_product.active_at')
                ->whereNotNull('vendor_outlets.active_at')
                ->whereNotNull('products.active_at')
                ->whereNotNull('product_categories.active_at')
                ->whereNull('products.deleted_at')
                ->whereNull('outlet_product.deleted_at')
                ->whereNull('vendor_outlets.deleted_at');

        $this->selectSub($categories_count_query, 'available_categories_count');

        return $this;
    }
}
