<?php

namespace App\Builders;

use App\Models\User;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\ProductCategory;

class OutletProductBuilder extends BaseBuilder
{
    public function active()
    {
        parent::activeForTable('outlet_product');

        return $this;
    }

    public function hasAvailableItemsViaProducts()
    {
        $this->where(function (OutletProductBuilder $query) {
            $query->whereHas('product', function (ProductBuilder $query) {
                $query->activeAsCategory();
            });
        });

        return $this;
    }

    public function hasAvailableItemsViaVendors()
    {
        $this->where(function (OutletProductBuilder $query) {
            $query->whereHas('outlet', function (VendorOutletBuilder $query) {
                $query->activeAsVendor();
            });
        });

        return $this;
    }

    public function available()
    {
        $this->where(function (OutletProductBuilder $query) {
            return $query->active()
                ->hasAvailableItemsViaProducts()
                ->hasAvailableItemsViaVendors();
        });

        return $this;
    }

    public function whereHasProductIsAvailable(Product $product)
    {
        $this->where(function (OutletProductBuilder $query) use ($product) {
            $query->available()
                ->whereHas('product', function ($query) use ($product) {
                    $query->where('products.id', $product->id);
                }
            );
        });

        return $this;
    }

    public function whereProductCategory(ProductCategory $category)
    {
        $this->where(function (OutletProductBuilder $query) use ($category) {
            $query->whereHas(
                'product.category',
                function ($query) use ($category) {
                    return $query->where('product_categories.id', $category->id);
                }
            );
        });

        return $this;
    }

    public function availableAsCategory(ProductCategory $category)
    {
        $this->where(function (OutletProductBuilder $query) use ($category) {
            $query->available()->whereProductCategory($category);
        });

        return $this;
    }

    public function availableAsVendor(Vendor $vendor)
    {
        $this->where(function (OutletProductBuilder $query) use ($vendor) {
            $query->available()->whereVendor($vendor);
        });

        return $this;
    }

    public function whereVendor(Vendor $vendor)
    {
        $this->where(function (OutletProductBuilder $query) use ($vendor) {
            $query->whereHas(
                'outlet.vendor',
                function ($query) use ($vendor) {
                    return $query->where('id', $vendor->id);
                }
            );
        });

        return $this;
    }

    public function whereIsUserFavourites(User $user)
    {
        $this->where(function (OutletProductBuilder $query) use ($user) {
            $query->whereHas('favourites', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            });
        });

        return $this;
    }

    public function withProductDetails()
    {
        $this->with(['product' => function ($query) {
            $query->with([
                'category',
                'vendorImages' => function ($query) {
                    $query->with('vendor:id,slug,name');
                }
            ]);
        }]);

        return $this;
    }

    public function withFavouritesCount(User $user = null)
    {
        $this->withCount(['favourites' => function ($query) use ($user) {
            $query->where('user_id', $user->id ?? null);
        }]);

        return $this;
    }

    public function withRatings()
    {
        $this->with('ratingsAverage')
            ->withCount('ratings');

        return $this;
    }
}
