<?php

namespace App\Builders;

use App\Models\Cart;
use App\Models\User;
use App\Models\Order;
use App\Models\PaymentCard;

class OrderBuilder extends BaseBuilder
{
    public function whereBelongsToUser(User $user)
    {
        $this->where(function (OrderBuilder $query) use ($user) {
            $query->whereHas('cart', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            });
        });

        return $this;
    }

    public function lastlyUsedActivePaymentMethod()
    {
        $this->where(function ($query) {
            $query->whereHasMorph(
                'payable',
                [PaymentCard::class],
                function ($query) {
                    $query->whereNotNull('active_at');
                }
            );
        })->latest();

        return $this;
    }

    public function whereHasCartsWithItemsOfStatus(string $status)
    {
        $this->where(function ($query) use ($status) {
            $query->whereHas('cart', function ($query) use ($status) {
                $query->has('items')
                    ->where('status', $status);
            });
        });

        return $this;
    }

    public function whereHasActiveCartWithItems()
    {
        return $this->whereHasCartsWithItemsOfStatus(
            Cart::STATUSES['ACTIVE']
        );
    }

    public function latestPendingForPaymentByUser(
        User $user, string $order = null
    ) {
        $this->where('status', Order::STATUSES['PENDING'])
            ->whereHasCartsWithItemsOfStatus(Cart::STATUSES['USED'])
            ->whereHas('cart', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
            ->when($order, function ($query, $order) {
                return $query->whereHashId($order);
            })
            ->latest();

        return $this;
    }

    public function whereAwaitingPaymentVerification()
    {
        $this->where([
            'status' => Order::STATUSES['PROCESSING'],
            'payment_status' => Order::PAYMENT_STATUSES['PENDING']
        ]);

        return $this;
    }

    public function whereNotCompleted()
    {
        $this->whereNotIn('status', [
            Order::STATUSES['DELIVERED'], Order::STATUSES['CANCELLED']
        ])
        ->orWhere('payment_status', Order::PAYMENT_STATUSES['PENDING']);

        return $this;
    }
}
