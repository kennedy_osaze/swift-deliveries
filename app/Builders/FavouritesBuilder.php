<?php

namespace App\Builders;

use App\Models\User;
use App\Models\Vendor;
use App\Models\OutletProduct;

class FavouritesBuilder extends BaseBuilder
{
    public function hasAvailableItems()
    {
        $this->whereHasMorph(
            'favouritable',
            OutletProduct::class,
            function ($query) {
                $query->available();
            }
        );

        return $this;
    }

    public function hasItemForVendor(Vendor $vendor)
    {
        $this->whereHasMorph(
            'favouritable',
            OutletProduct::class,
            function ($query) use ($vendor) {
                $query->whereHas('outlet', function ($query) use ($vendor) {
                    return $query->where('vendor_id', $vendor->id);
                });
            }
        );

        return $this;
    }

    public function hasItemsForUser(User $user)
    {
        $this->whereHasMorph(
            'favouritable',
            OutletProduct::class,
            function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }
        );

        return $this;
    }

    public function withOutletItemDetails($query)
    {
        $this->with(['favouritable' => function ($query) {
            $query->withCount('ratings')
                ->with('item.category', 'outlet.vendor', 'ratingsAverage');
        }]);

        return $this;
    }
}
