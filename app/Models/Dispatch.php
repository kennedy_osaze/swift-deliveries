<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dispatch extends Model
{
    use SoftDeletes;

    public const TRANSPORT_TYPES = [
        'CAR' => 1,
        'MOTOR CYCLE' => 2,
        'BICYCLE' => 3,
        'SCOOTER' => 4,
        'FOOT' => 5,
        'TRUCK' => 6
    ];

    public const FLEET_TYPES = [
        'CAPTIVE' => 1,
        'FREELANCE' => 2
    ];

    protected $fillable = [
        'firstname', 'lastname','username','email','password','phone','transport_type','fleet_type','fleet_id'
    ];

}
