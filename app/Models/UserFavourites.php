<?php

namespace App\Models;

use App\Builders\FavouritesBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavourites extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'favouritable_type', 'favouritable_id',
    ];

    public function newEloquentBuilder($query)
    {
        return new FavouritesBuilder($query);
    }

    public function favouritable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Gets the classes whose objects can be ratable
     * Use for knowledge purposes.
     *
     * @return array
     */
    public static function morphableTos()
    {
        return [
            'outlet_product',
        ];
    }
}
