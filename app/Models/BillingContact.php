<?php

namespace App\Models;

use Mtvs\EloquentHashids\HasHashid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillingContact extends Model
{
    use SoftDeletes, HasHashid;

    protected $fillable = [
        'user_id', 'address', 'longitude', 'latitude', 'active_at'
    ];

    protected $casts = [
        'active_at' => 'datetime',
        'latitude' => 'decimal:8',
        'longitude' => 'decimal:8',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDetails()
    {
        return [
            'id' => $this->hashid(),
            'address' => $this->address,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'active' => ! is_null($this->active_at),
        ];
    }

    /**
     * Sets a contact as active
     *
     * @param static $contact
     *
     * @return bool
     */
    public static function setContactAsActive(BillingContact $contact)
    {
        $current_active_contact = static::whereNotNull('active_at')
            ->where('user_id', $contact->user_id)
            ->first();

        if ($contact->is($current_active_contact)) {
            return true;
        }

        if (! is_null($current_active_contact)) {
            $current_active_contact->update(['active_at' => null]);
        }

        return $contact->update(['active_at' => now()]);
    }
}
