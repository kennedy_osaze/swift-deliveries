<?php

namespace App\Models;

use App\Builders\VendorBuilder;
use Mtvs\EloquentHashids\HasHashid;
use App\Contracts\SluggableContract;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model implements SluggableContract
{
    use SoftDeletes, Sluggable, HasHashid;

    /**
     * List od vendor types
     *
     * @var array
     */
    public const TYPES = [
        'RESTAURANT' => 'restaurant',
    ];

    protected $fillable = [
        'name', 'slug', 'description', 'vendor_type', 'logo', 'active_at',
    ];

    protected $casts = [
        'active_at' => 'datetime'
    ];

    public function newEloquentBuilder($query)
    {
        return new VendorBuilder($query);
    }

    public function sluggable()
    {
        return [
            $this->sluggableKeyName() => ['source' => 'name']
        ];
    }

    public function sluggableKeyName()
    {
        return 'slug';
    }

    public function outlets()
    {
        return $this->hasMany(VendorOutlet::class);
    }

    public function vendorAdmin()
    {
        return $this->hasOne(VendorAdmin::class, 'vendor_id');
    }

    public function outletProducts()
    {
        return $this->hasManyThrough(
            OutletProduct::class, VendorOutlet::class, 'vendor_id', 'outlet_id'
        );
    }

    public function customRelationProductCategories()
    {
        $relation = $this->outletProducts();

        $relation->getQuery()
            ->join('products', 'products.id', '=', 'outlet_product.product_id')
            ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
            ->select('product_categories.*')
            ->distinct();

        return $relation;
    }

    public function productImages()
    {
        return $this->hasMany(VendorProductImage::class);
    }

    public function randomProductImage()
    {
        return $this->hasOne(VendorProductImage::class)->inRandomOrder();
    }
}
