<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorProductImage extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'vendor_id','product_id', 'item_id', 'image',
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function scopeHasActiveVendor(Builder $query)
    {
        return $query->where(function ($query) {
            $query->whereHas('vendor', function ($query) {
                $query->available();
            });
        });
    }
}
