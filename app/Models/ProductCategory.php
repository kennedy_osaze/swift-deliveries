<?php

namespace App\Models;

use Mtvs\EloquentHashids\HasHashid;
use App\Contracts\SluggableContract;
use Illuminate\Database\Eloquent\Model;
use App\Builders\ProductCategoryBuilder;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model implements SluggableContract
{
    use SoftDeletes, Sluggable, HasHashid;

    /**
     * The list of products types
     *
     * @var array
     */
    public const PRODUCT_TYPES = [
        'DRINK' => 'drink',
        'FOOD' => 'food',
    ];

    protected $fillable = [
        'name', 'product_type', 'slug', 'image', 'active_at',
    ];

    protected $casts = [
        'active_at' => 'datetime',
    ];

    public function newEloquentBuilder($query)
    {
        return new ProductCategoryBuilder($query);
    }

    public function sluggable()
    {
        return [
            $this->sluggableKeyName() => ['source' => 'name']
        ];
    }

    public function sluggableKeyName()
    {
        return 'slug';
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function outletProducts()
    {
        return $this->hasManyThrough(
            OutletProduct::class, Product::class, 'category_id', 'product_id'
        );
    }

    public function customRelationVendors()
    {
        $relation = $this->outletProducts();

        $relation->getQuery()
            ->join('vendor_outlets', 'vendor_outlets.id', '=', 'outlet_product.outlet_id')
            ->join('vendors', 'vendors.id', '=', 'vendor_outlets.vendor_id')
            ->select('vendors.*')
            ->distinct();

        return $relation;
    }
}
