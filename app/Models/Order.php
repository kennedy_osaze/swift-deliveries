<?php

namespace App\Models;

use App\Services\Paystack;
use Illuminate\Support\Str;
use App\Builders\OrderBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mtvs\EloquentHashids\HasHashid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes, HasHashid;

    private const REFERENCE_PREFIX = 'ORD_';

    public const STATUSES = [
        'PENDING' => 'pending',
        'PROCESSING' => 'processing',
        'READY' => 'ready',
        'DELIVERED' => 'delivered',
        'CANCELLED' => 'cancelled',
    ];

    public const PAYMENT_STATUSES = [
        'PENDING' => 'pending',
        'PAID' => 'paid',
        'DECLINED' => 'declined',
    ];

    public const PAYMENT_TYPES = [
        'CARD' => 'payment_card',
    ];

    protected $fillable = [
        'cart_id', 'total_amount', 'reference_code', 'payable_type',
        'status', 'payable_id', 'payment_status', 'paid_at', 'contact_id',
    ];

    protected $casts = [
        'paid_at' => 'datetime',
    ];

    public function newEloquentBuilder($query)
    {
        return new OrderBuilder($query);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function billingContact()
    {
        return $this->belongsTo(BillingContact::class, 'contact_id');
    }

    /**
     * Gets the payment method model
     */
    public function payable()
    {
        return $this->morphTo();
    }

    /**
     * Scope to find an order by a payment reference code
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $reference
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWherePaymentReference(
        $query,
        string $reference_without_prefix
    ) {
        return $query->where([
            'reference_code' => static::REFERENCE_PREFIX . $reference_without_prefix
        ]);
    }

    /**
     * Get the reference code to be used for payment
     *
     * @return string
     */
    public function getPaymentReferenceCodeAttribute()
    {
        return Str::replaceFirst(
            static::REFERENCE_PREFIX, '', $this->reference_code
        );
    }

    /**
     * Get details about an order
     *
     * @return array
     */
    public function getDetails()
    {
        $this->loadMissing(
            'cart.items.outletProduct', 'payable', 'billingContact'
        );

        $cart = $this->cart->items->map(function ($item) {
            return $item->getBasicDetails();
        });

        $payment_details = (! $this->payable) ? null : [
            'id' => $this->payable->hashid(),
            'type' => $this->payable_type,
            'details' => $this->payable->getDetails(),
        ];

        return [
            'id' => $this->hashid(),
            'reference_code' => $this->reference_code,
            'total_amount' => $this->total_amount,
            'created_at' => (string) $this->created_at,
            'cart' => $cart,
            'payment' => $payment_details,
            'billing_contact' => (! $this->billingContact)
                ? null : $this->billingContact->getDetails(),
        ];
    }

    /**
     * Generates a reference code for an order
     *
     * @return string
     */
    public static function generateReferenceCode()
    {
        $reference_code = static::REFERENCE_PREFIX
            . Str::random(config('settings.order.reference_length'));

        // Regenerate the reference code if it exists
        if (static::where('reference_code', $reference_code)->exists()) {
            return static::generateReferenceCode();
        }

        return $reference_code;
    }

    /**
     * Completes the payment process for an order
     * It uses the data from a payment provider for that purpose
     *
     * @param array $payment_data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function completePayment(array $payment_data)
    {
        return DB::transaction(function () use ($payment_data) {
            try {
                $user = $this->cart->owner;
                $auth_code = data_get($payment_data, 'authorization.authorization_code' ?? '');

                $payment_card = $user->paymentCards()->firstOrCreate(
                    ['authorization_code' => $auth_code],
                    array_merge(
                        Paystack::getPaymentCardDetails($payment_data),
                        ['active_at' => now()]
                    )
                );

                return $this->updateAfterPaymentMadeWithCard($payment_card);
            } catch (\Exception $e) {
                $error_message =
                    "An error occurred completing user: {$user->id} payment transaction:
                    {$e->getMessage()}";
                Log::error($error_message, $e->getTrace());

                throw new \Exception($error_message, 0, $e);
            }
        });
    }

    /**
     * Update the order record after payment has been made with a card
     *
     * @param \App\Models\PaymentCard $card
     *
     * @return bool
     */
    public function updateAfterPaymentMadeWithCard(PaymentCard $card)
    {
        return $this->update([
            'status' => Order::STATUSES['PROCESSING'],
            'payable_type' => Order::PAYMENT_TYPES['CARD'],
            'payable_id' => $card->id,
            'payment_status' => Order::PAYMENT_STATUSES['PAID'],
            'paid_at' => now()
        ]);
    }

    /**
     * Gets the classes whose objects can be payable
     * Use for knowledge purposes.
     *
     * @return array
     */
    private static function classAliasesPayable()
    {
        return [
            'payment_card', // For \App\Models\PaymentCard::class
        ];
    }
}
