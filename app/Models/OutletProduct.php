<?php

namespace App\Models;

use App\Traits\Filterable;
use Laravel\Scout\Searchable;
use Mtvs\EloquentHashids\HasHashid;
use App\Builders\OutletProductBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutletProduct extends Model
{
    use SoftDeletes, HasHashid, Filterable, Searchable;

    protected $table = 'outlet_product';

    protected $fillable = [
        'outlet_id', 'product_id', 'price', 'active_at',
    ];

    protected $casts = [
        'price' => 'decimal:2',
    ];

    public function newEloquentBuilder($query)
    {
        return new OutletProductBuilder($query);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function outlet()
    {
        return $this->belongsTo(VendorOutlet::class);
    }

    public function favourites()
    {
        return $this->morphMany(UserFavourites::class, 'favouritable');
    }

    public function ratings()
    {
        return $this->morphMany(UserRating::class, 'ratable');
    }

    public function ratingsAverage()
    {
        return $this->ratings()
            ->selectRaw("avg(rating) as outlet_item_average, ratable_id")
            ->groupBy('ratable_id');
    }

    public function favouredBy()
    {
        return $this->morphToMany(
            User::class, 'favouritable', 'user_favourites',
        );
    }

    public function ratedBy()
    {
        return $this->morphToMany(
            User::class, 'ratable', 'user_ratings',
        );
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class, 'cart_items', 'item_id', 'cart_id')
            ->withPivot('name', 'quantity', 'price', 'comment')
            ->withTimestamps();
    }

    public function searchableAs()
    {
        return 'outlet_products_index';
    }

    public function toSearchableArray()
    {
        $this->loadMissing(['product.category', 'outlet.vendor']);

        $product = $this->product;
        $category = $product->category;
        $outlet = $this->outlet;
        $vendor = $outlet->vendor;

        return [
            'id' => $this->id,
            'price' => $this->price,
            'product_id' => $this->product_id,
            'product_name' => $product->name,
            'product_description' => $product->description,
            'category_type' => $category->product_type,
            'category_name' => $category->name,
            'outlet_id' => $this->outlet_id,
            'outlet_address' => $outlet->address,
            'outlet_city' => $outlet->city,
            'outlet_state' => $outlet->state,
            'vendor_name' => $vendor->name,
            'vendor_description' => $vendor->description,
            'vendor_type' => $vendor->vendor_type,
        ];
    }

    /**
     * Checks if the product item is a user's favourites
     *
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function isFavouredBy(User $user)
    {
        return $this->favouredBy()
            ->where('users.id', $user->id)
            ->exists();
    }

    /**
     * Adds the product item as a user's favourite
     *
     * @param \App\Models\User @user
     *
     * @return \App\Models\UserFavourites
     */
    public function addAsUserFavourite(User $user)
    {
        return $this->favourites()->create(['user_id' => $user->id]);
    }

    /**
     * Get a generic name for the item.
     *
     * @return string
     */
    public function getGenericNameAttribute()
    {
        $outlet = $this->outlet;
        $vendor = $outlet->vendor;
        $product = $this->product;

        return "{$product->name} from {$vendor->name}, {$outlet->city}";
    }

    /**
     * Removes the product item from a user's favourites list
     *
     * @param \App\Models\User @user
     *
     * @return bool
     */
    public function removeUserFavourite(User $user)
    {
        return (bool) $this->favourites()
            ->where('user_id', $user->id)
            ->update(['deleted_at' => now()->toDateTimeString()]);
    }
}
