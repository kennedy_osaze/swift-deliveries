<?php

namespace App\Models;

use Mtvs\EloquentHashids\HasHashid;
use App\Builders\VendorOutletBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorOutlet extends Model
{
    use SoftDeletes, HasHashid;

    protected $fillable = [
        'vendor_id', 'latitude', 'longitude', 'address', 'city', 'state',
        'active_at',
    ];

    protected $casts = [
        'latitude' => 'decimal:8',
        'longitude' => 'decimal:8',
        'active_at' => 'datetime',
    ];

    public function newEloquentBuilder($query)
    {
        return new VendorOutletBuilder($query);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function products()
    {
        return $this->belongsToMany(
            Product::class, 'outlet_product', 'outlet_id', 'product_id'
        )->withPivot('price', 'active_at')->withTimestamps();
    }

    public function ratings()
    {
        return $this->hasManyThrough(
            UserRating::class,
            OutletProduct::class,
            'outlet_id',
            'ratable_id'
        )->where('user_ratings.ratable_type', 'outlet_product');
    }

    public function ratingsAverage()
    {
        return $this->ratings()
            ->selectRaw("avg(rating) as outlet_product_average, ratable_id")
            ->groupBy('ratable_id');
    }

    /**
     * Pivot Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outletProductPivot()
    {
        return $this->hasMany(OutletProduct::class, 'outlet_id');
    }

    public function admin()
    {
        return $this->hasOne(VendorAdmin::class, 'outlet_id');
    }
}
