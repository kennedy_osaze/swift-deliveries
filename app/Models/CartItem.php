<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'cart_id', 'item_id', 'name', 'quantity', 'price', 'comment',
    ];

    protected $casts = [
        'quantity' => 'integer',
        'price' => 'decimal:2',
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function outletProduct()
    {
        return $this->belongsTo(OutletProduct::class, 'item_id');
    }

    /**
     * Get the basic details about an item in a cart
     *
     * @return array
     */
    public function getBasicDetails()
    {
        $item = $this->outletProduct;

        return [
            'item' => $item->hashid(),
            'name' => $this->name,
            'quantity' => $this->quantity,
            'price' => $item->price,
            'comment' => $this->comment,
            'created_at' => (string) $this->created_at
        ];
    }

    public function getLongDetails()
    {
        $this->loadMissing(['outletProduct' => function ($query) {
            $query->with('outlet.vendor', 'product');
        }]);

        $item = $this->outletProduct;
        $product = $item->product;
        $outlet = $item->outlet;
        $vendor = $outlet->vendor;

        return $this->getBasicDetails() + [
            'product' => [
                'id' => $product->hashid(),
                'slug' => $product->slug,
                'name' => $product->name,
                'description' => $product->description,
            ],
            'vendor' => [
                'id' => $vendor->hashid(),
                'slug' => $vendor->slug,
                'name' => $vendor->name,
                'outlet' => [
                    'id' => $outlet->hashid(),
                    'address' => $outlet->address,
                    'city' => $outlet->city,
                    'state' => $outlet->state,
                ],
            ],
        ];
    }
}