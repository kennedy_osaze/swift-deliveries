<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;

    public const STATUSES = [
        'ACTIVE' => 'active',
        'USED' => 'used',
    ];

    protected $fillable = [
        'user_id', 'status',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    public function order()
    {
        return $this->hasOne(Order::class);
    }

    public function outletProducts()
    {
        return $this->belongsToMany(OutletProduct::class, 'cart_items', 'cart_id', 'item_id');
    }

    /**
     * Gets a user's active active cart if it exist or creates it
     *
     * @return static
     */
    public static function getOrCreateUserActiveCart(User $user)
    {
        return $user->activeCart()->firstOrCreate([
            'status' => static::STATUSES['ACTIVE']
        ]);
    }

    /**
     * Add a product item to user cart
     *
     * @param \App\Models\User $user
     * @param array $data
     *
     * @return \App\Models\CartItem
     */
    public static function addItem(User $user, array $data)
    {
        $cart = static::getOrCreateUserActiveCart($user);

        $product_item = OutletProduct::whereHashId($data['item'])->first();

        return $cart->items()->create([
            'item_id' => $product_item->id,
            'name' => $product_item->generic_name,
            'quantity' => $data['quantity'],
            'price' => $product_item->price,
            'comment' => $data['comment'] ?? null,
        ]);
    }

    /**
     * Add multiple product items to a user cart
     *
     * @param \App\Models\User $user
     * @param \Illuminate\Support\Collection $items The collection of product item details to add to cart
     *
     * @return static
     */
    public static function addMultipleItems(User $user, Collection $items)
    {
        $cart = static::getOrCreateUserActiveCart($user);

        return DB::transaction(function () use ($cart, $items) {
            try {
                $item_ids = $items->pluck('id')->toArray();

                $product_items_cursor = OutletProduct::with('outlet.vendor', 'product')
                    ->whereIn('id', $item_ids)
                    ->cursor();

                foreach ($product_items_cursor as $product_item) {
                    $cart_item_details = $items->where('id', $product_item->id)->first();

                    $product_item->carts()->attach($cart->id, [
                        'name' => $product_item->generic_name,
                        'quantity' => $cart_item_details['quantity'],
                        'price' => $product_item->price,
                        'comment' => $cart_item_details['comment'] ?? null,
                    ]);
                }

                return $cart;
            } catch (\Exception $e) {
                throw new \Exception(
                    "Unable to add multiple cart items: {$e->getMessage()}", 0, $e
                );
            }
        });
    }

    /**
     * Determines if an item can be added to cart
     * Checks if the item belongs to the same outlet as previously added items
     *
     * @param string $item
     *
     * @return bool
     */
    public function canAddItemWithOutletAsOtherItemsAdded(string $item)
    {
        if (! $item = OutletProduct::whereHashIdOrSlug($item)->first()) {
            return false;
        }

        return $this->items()
            ->whereHas('outletProduct', function ($query) use ($item) {
                $query->where('outlet_id', $item->outlet_id);
            })
            ->exists();
    }

    /**
     * Creates an order for an active cart when checking out
     *
     * @return \App\Models\Order
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function createOrder()
    {
        $not_active = $this->status !== static::STATUSES['ACTIVE'];
        abort_if($not_active, 400, 'cart_already_used');

        return DB::transaction(function () {
            try {
                $total_items_amount = $this->items->sum(function ($item) {
                    return $item->quantity * $item->price;
                });

                $user = $this->owner;
                $last_order_with_active_payable = Order::whereBelongsToUser($user)
                    ->lastlyUsedActivePaymentMethod()
                    ->first();

                $this->update(['status' => static::STATUSES['USED']]);

                return $this->order()->create([
                    'reference_code' => Order::generateReferenceCode(),
                    'total_amount' => $total_items_amount,
                    'payable_type' => $last_order_with_active_payable->type ?? null,
                    'payable_id' => $last_order_with_active_payable->id ?? null,
                    'contact_id' => $user->activeBillingContact->id ?? null,
                ]);
            } catch (\Exception $e) {
                throw new \Exception(
                    "Error creating order: {$e->getMessage()}", $e->getCode(), $e
                );
            }
        });
    }
}
