<?php

namespace App\Models;

use App\Builders\RatingsBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRating extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'ratable_type', 'ratable_id', 'rating', 'comment',
    ];

    public function ratable()
    {
        return $this->morphTo();
    }

    public function newEloquentBuilder($query)
    {
        return new RatingsBuilder($query);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Gets the classes whose objects can be ratable
     * Use for knowledge purposes.
     *
     * @return array
     */
    private static function classAliasesRatable()
    {
        return [
            'outlet_product',
        ];
    }
}
