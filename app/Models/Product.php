<?php

namespace App\Models;

use App\Builders\ProductBuilder;
use Mtvs\EloquentHashids\HasHashid;
use App\Contracts\SluggableContract;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements SluggableContract
{
    use SoftDeletes, Sluggable, HasHashid;

    protected $fillable = [
        'category_id', 'name', 'slug', 'image', 'description', 'active_at',
    ];

    public function sluggable()
    {
        return [
            $this->sluggableKeyName() => ['source' => 'name']
        ];
    }

    public function sluggableKeyName()
    {
        return 'slug';
    }

    public function newEloquentBuilder($query)
    {
        return new ProductBuilder($query);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function vendorsOfVendorProductImages()
    {
        return $this->belongsToMany(Vendor::class, 'vendor_product_images');
    }

    public function vendorImages()
    {
        return $this->hasMany(VendorProductImage::class);
    }

    public function randomVendorProductImage()
    {
        return $this->hasOne(VendorProductImage::class)->inRandomOrder();
    }

    public function outlets()
    {
        return $this->belongsToMany(
            VendorOutlet::class, 'outlet_product', 'product_id', 'outlet_id'
        )->withPivot('price', 'active_at')->withTimestamps();
    }

    /**
     * Pivot Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function outletProductPivot()
    {
        return $this->hasMany(OutletProduct::class);
    }

    public function favourites()
    {
        return $this->hasManyThrough(
            UserFavourites::class,
            OutletProduct::class,
            'product_id',
            'favouritable_id'
        )->where('user_favourites.favouritable_type', 'outlet_product');
    }

    public function ratings()
    {
        return $this->hasManyThrough(
            UserRating::class,
            OutletProduct::class,
            'product_id',
            'ratable_id'
        )->where('user_ratings.ratable_type', 'outlet_product');
    }

    public function ratingsAverage()
    {
        return $this->ratings()
            ->selectRaw("avg(rating) as outlet_product_average, ratable_id")
            ->groupBy('ratable_id');
    }
}
