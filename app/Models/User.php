<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use SoftDeletes, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone',
        'password', 'remember_token', 'profile_picture',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'password', 'remember_token', 'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function favourites()
    {
        return $this->hasMany(UserFavourites::class);
    }

    public function favouredOutletProducts()
    {
        return $this->morphedByMany(
            OutletProduct::class, 'favouritable', 'user_favourites',
        )->withTimestamps();
    }

    public function ratings()
    {
        return $this->hasMany(UserRating::class);
    }

    public function ratedOutletProducts()
    {
        return $this->morphedByMany(
            OutletProduct::class, 'ratable', 'user_ratings',
        )->withPivot('rating', 'comment')->withTimestamps();
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function activeCart()
    {
        return $this->hasOne(Cart::class)
            ->where('status', Cart::STATUSES['ACTIVE'])
            ->latest();
    }

    public function paymentCards()
    {
        return $this->hasMany(PaymentCard::class);
    }

    public function activePaymentCard()
    {
        return $this->hasOne(PaymentCard::class)
            ->whereNotNull('active_at')
            ->latest();
    }

    public function orders()
    {
        return $this->hasManyThrough(Order::class, Cart::class);
    }

    public function billingContacts()
    {
        return $this->hasMany(BillingContact::class);
    }

    public function activeBillingContact()
    {
        return $this->hasOne(BillingContact::class)
            ->whereNotNull('active_at')
            ->latest();
    }
}
