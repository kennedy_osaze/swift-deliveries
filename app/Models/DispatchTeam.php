<?php

namespace App\Models;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DispatchTeam extends Model
{
    use SoftDeletes;

    public const BATTERY_USAGE = [
        'LOW' => 1,
        'MEDIUM' => 2,
        'HIGH' => 3
    ];

    protected $fillable = [
        'name', 'team_id','battery_usage','tags'
    ];

}
