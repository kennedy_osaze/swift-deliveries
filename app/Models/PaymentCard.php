<?php

namespace App\Models;

use Mtvs\EloquentHashids\HasHashid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentCard extends Model
{
    use SoftDeletes, HasHashid;

    protected $fillable = [
        'user_id', 'email', 'authorization_code', 'card_type',
        'last_four_digits', 'expiry_month', 'expiry_year', 'is_reusable',
        'active_at',
    ];

    protected $casts = [
        'active_at' => 'datetime',
        'is_reusable' => 'boolean',
    ];

    public function orders()
    {
        return $this->morphMany(Order::class, 'payable');
    }

    /**
     * Get a payment card details
     *
     * @return array
     */
    public function getDetails()
    {
        return [
            'id' => $this->hashid(),
            'active' => ! is_null($this->active_at)
        ]
        + $this->only(
            'email', 'authorization_code', 'last_four_digits', 'card_type',
            'is_reusable'
        );
    }
}
