<?php

namespace App\Http\Resources;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Traits\SubstituteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategory extends JsonResource
{
    use SubstituteResourceCollection;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $products = $this->products;
        $vendor_product_image = $products->random()->randomVendorProductImage;
        $average_ratings = $products->pluck('ratingsAverage')->collapse();

        return [
            'id' => $this->hashid(),
            'slug' => $this->slug,
            'name' => $this->name,
            'product_type' => $this->product_type,
            'image' => $this->image
                ? Storage::disk('product-categories')->url($this->image)
                : null,
            'products_available_count' => $products->count(),
            'created_at' => (string) $this->created_at,
            'available_items' => $this->available_items ?? 0,
            'available_vendors_count' => $this->when(
                ! $this->relationLoaded('customRelationVendors'),
                $this->available_vendors_count
            ),
            'total_ratings_count' => $products->sum('ratings_count'),
            'total_ratings_average' =>
                $average_ratings->avg('outlet_product_average')
                ?? 0,
            'random_vendor_item_image' => $vendor_product_image
                ? Storage::disk('product-items')->url($vendor_product_image->image)
                : null,
            'vendors' => $this->whenLoaded('customRelationVendors', function () {
                return $this->vendorsDetails($this->customRelationVendors);
            })
        ];
    }

    private function vendorsDetails(Collection $vendors)
    {
        return $vendors->map(function ($vendor) {
            return [
                'id' => hash_id_encode($vendor->id),
                'slug' => $vendor->slug,
                'name' => $vendor->name,
            ];
        });
    }

    public function with($request)
    {
        return [
            'status' => true,
            'message' => 'The product category has been retrieved successfully.',
        ];
    }

    protected static function metaData()
    {
        return [
            'response_message' => 'List of product categories retrieved successfully.',
        ];
    }
}
