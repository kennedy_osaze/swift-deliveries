<?php

namespace App\Http\Resources;

use App\Traits\SubstituteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;


class Order extends JsonResource
{
    use SubstituteResourceCollection;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getDetails() + [
            'status' => $this->status,
            'payment_status' => $this->payment_status,
            'paid_at' => (string) $this->paid_at ?: null
        ];
    }

    public function with($request)
    {
        return [
            'status' => true,
            'message' => 'The order has be retrieved successfully.',
        ];
    }

    protected static function metaData()
    {
        return [
            'response_message' => 'The orders has been retrieved successfully.',
        ];
    }
}
