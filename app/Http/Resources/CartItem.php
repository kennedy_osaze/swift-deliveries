<?php

namespace App\Http\Resources;

use App\Traits\SubstituteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItem extends JsonResource
{
    use SubstituteResourceCollection;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->getLongDetails();
    }

    public function additional($array)
    {
        return [
            'status' => true,
            'message' => 'The cart item has be retrieved successfully.',
        ];
    }

    protected static function metaData()
    {
        return [
            'response_message' => 'The cart items has been retrieved successfully.',
        ];
    }
}
