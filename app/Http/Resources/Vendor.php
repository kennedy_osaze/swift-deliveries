<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Traits\SubstituteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class Vendor extends JsonResource
{
    use SubstituteResourceCollection;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $outlets = $this->outlets;

        $random_item_image = optional($this->randomProductImage)->image;
        $average_ratings = $outlets->pluck('ratingsAverage')->collapse();

        return [
            'id' => $this->hashid(),
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'vendor_type' => $this->vendor_type,
            'logo' => $this->logo
                ? Storage::disk('vendor-logos')->url($this->logo)
                : null,
            'created_at' => (string) $this->created_at,
            'available_items_count' => $this->available_items_count,
            'random_vendor_item_image' => $random_item_image
                ? Storage::disk('product-items')->url($random_item_image)
                : null,
            'available_outlets_counts' => $outlets->count(),
            'available_categories_count' => $this->when(
                ! $this->relationLoaded('customRelationProductCategories'),
                $this->available_categories_count ?? 0
            ),
            'total_ratings_count' => $outlets->sum('ratings_count'),
            'total_ratings_average' =>
                $average_ratings->avg('outlet_product_average') ?? 0,
            'product_categories' => $this->whenLoaded(
                'customRelationProductCategories',
                function () {
                    return $this->productCategoriesDetails(
                        $this->customRelationProductCategories
                    );
                }
            ),
            $this->mergeWhen(
                $this->requestRequiresOneVendor($request),
                ['outlets' => $this->outletsDetails($this->outlets)]
            ),
        ];
    }

    private function productCategoriesDetails(Collection $product_categories)
    {
        return $product_categories->map(function ($category) {
            return [
                'id' => hash_id_encode($category->id),
                'slug' => $category->slug,
                'name' => $category->name,
            ];
        });
    }

    private function outletsDetails(Collection $outlets)
    {
        return $outlets->map(function ($outlet) {
            return [
                'id' => $outlet->id,
                'address' => $outlet->address,
                'city' => $outlet->city,
                'state' => $outlet->state,
            ];
        });
    }

    /**
     * Determines whether the request route tries to
     * access a single vendor resource
     *
     * @param \Illuminate\Http\Request
     *
     * @return bool
     */
    private function requestRequiresOneVendor(Request $request)
    {
        return Str::contains($request->route()->getName(), 'api.vendor.show');
    }

    public function additional($array)
    {
        return [
            'status' => true,
            'message' => 'The vendor has be retrieved successfully.',
        ];
    }

    protected static function metaData()
    {
        return [
            'response_message' => 'The vendors have been retrieved successfully.',
        ];
    }
}
