<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection
    as BaseCollection;

class AnonymousResourceCollection extends BaseCollection
{
    /**
     * An array to hold custom meta data.
     *
     * @var mixed
     */
    private $meta_data = [];

    /**
     * Create a new anonymous resource collection.
     *
     * @param  mixed  $resource
     * @param  string  $collects
     *
     * @return void
     */
    public function __construct($resource, $collects, array $meta_data)
    {
        $this->meta_data = $meta_data;

        parent::__construct($resource, $collects);
    }

    /**
     * Customize the response for a request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\JsonResponse  $response
     *
     * @return void
     */
    public function withResponse($request, $response)
    {
        $original_response = clone $response;
        $response_data = $original_response->getData(true);

        $response->setData(
            array_merge([
                'success' => true,
                'message' => $this->meta_data['response_message'] ?? '',
            ], $response_data)
        );
    }
}
