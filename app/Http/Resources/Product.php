<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;
use App\Traits\SubstituteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    use SubstituteResourceCollection;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $vendor_product_image = $this->randomVendorProductImage;
        $average_ratings = $this->ratingsAverage;

        return [
            'id' => $this->hashid(),
            'slug' => $this->slug,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image
                ? Storage::disk('product-items')->url($this->image)
                : null,
            'created_at' => (string) $this->created_at,
            'category' => [
                'id' => $this->category->hashid(),
                'slug' => $this->category->slug,
                'name' => $this->name,
            ],
            'available_vendors_count' => $this->when(
                $this->available_vendors_count !== null,
                $this->available_vendors_count
            ),
            'available_items' => $this->outlet_product_pivot_count,
            'available_outlets_count' => $this->outlets_count,
            'ratings_count' => $this->ratings_count,
            'total_ratings_average' =>
                $average_ratings->avg('outlet_product_average') ?? 0,
            'random_vendor_item_image' => $vendor_product_image
                ? Storage::disk('product-items')->url($vendor_product_image->image)
                : null,

        ];
    }

    public function additional($array)
    {
        return [
            'status' => true,
            'message' => 'The product has be retrieved successfully.',
        ];
    }

    protected static function metaData()
    {
        return [
            'response_message' => 'The products have been retrieved successfully.',
        ];
    }
}
