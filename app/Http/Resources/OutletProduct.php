<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;
use App\Traits\SubstituteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class OutletProduct extends JsonResource
{
    use SubstituteResourceCollection;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = $this->product;
        $category = $product->category;

        $outlet = $this->outlet;
        $vendor = $outlet->vendor;

        return [
            'id' => $this->hashid(),
            'price' => $this->price,
            'created_at' => (string) $this->created_at,
            'ratings_count' => $this->ratings_count,
            'ratings_average' =>
                $this->ratingsAverage->avg('outlet_item_average') ?? 0,
            'favourites_count' => $this->when(
                $this->favourites_count !== null,
                $this->favourites_count
            ),
            'active' => $this->active_at !== null,
            'product' => [
                'id' => $product->hashid(),
                'slug' => $product->slug,
                'name' => $product->name,
                'category' => [
                    'id' => $category->hashid(),
                    'slug' => $category->slug,
                    'name' => $category->name,
                ],
            ],
            'vendor' => [
                'id' => $vendor->hashid(),
                'slug' => $vendor->slug,
                'name' => $vendor->name,
                'outlet' => [
                    'id' => $outlet->hashid(),
                    'address' => $outlet->address,
                    'city' => $outlet->city,
                    'state' => $outlet->state,
                ],
            ],
            'images' => $product->vendorImages
                ->where('vendor_id', $vendor->id)
                ->map(function ($item) {
                    return Storage::disk('product-items')->url($item->image);
                })
                ->all(),
        ];
    }

    public function additional($array)
    {
        return [
            'status' => true,
            'message' => 'The item has be retrieved successfully.',
        ];
    }

    protected static function metaData()
    {
        return [
            'response_message' => 'The product items been retrieved successfully.',
        ];
    }
}
