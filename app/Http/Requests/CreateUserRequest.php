<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'min:3', 'max:100'],
            'last_name' => ['required', 'string', 'min:3', 'max:100'],
            'email' => ['required', 'email', 'unique:users'],
            'phone' => ['bail', 'required', 'phone:NG',
                function ($attribute, $value, $fail) {
                    if (User::where('phone', $value)->exists()) {
                        $fail("The {$attribute} has already been taken.");
                    }
                }
            ],
            "password" => ['required', 'string', 'min:8'],
        ];
    }
}
