<?php

namespace App\Http\Requests;

use App\Traits\CanAddCartItemViaRequest;
use Illuminate\Foundation\Http\FormRequest;

class SaveCartItemRequest extends FormRequest
{
    use CanAddCartItemViaRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'quantity' => 'required|integer|min:1|max:1000',
            'comment' => 'nullable|string|min_words:3|max:30000',
        ];

        if ($this->isMethod('post')) {
            $item_rule = [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if ($this->doesCartItemExistsInUserCart((string) $value)) {
                        $fail(trans('validation.cart_item_exists.single', compact('attribute')));
                    }
                },
                function ($attribute, $value, $fail) {
                    if (! $this->isItemAvailable((string) $value)) {
                        $fail(trans('validation.not_available.single', compact('attribute')));
                    }
                },
                function ($attribute, $value, $fail) {
                    if (! $this->itemIsOfSameOutlet((string) $value)) {
                        $fail(trans('validation.item_not_same_outlet.single', compact('attribute')));
                    }
                },
            ];

            $rules += ['item' => $item_rule];
        }

        return $rules;
    }

    /**
     * Check if the item has been added to the user cart already
     *
     * @param string $item
     *
     * @return bool
     */
    private function doesCartItemExistsInUserCart(string $item)
    {
        return Auth::user()->activeCart()
            ->whereHas('items', function ($query) use ($item) {
                $query->where('item_id', hash_id_decode($item));
            })
            ->exists();
    }

    /**
     * Check if the item is available for sale
     *
     * @param string $item
     *
     * @return bool
     */
    private function isItemAvailable(string $item)
    {
        return OutletProduct::whereHashId($item)->available()->exists();
    }

    /**
     * Checks if an item is of the same outlet as the ones already added
     *
     * @param string $item
     *
     * @return bool
     */
    private function itemIsOfSameOutlet(string $item)
    {
        $cart = Auth::user()->activeCart;

        if (! $cart || $cart->items()->doesntExist()) {
            return true;
        }

        return $cart->canAddItemWithOutletAsOtherItemsAdded($item);
    }
}
