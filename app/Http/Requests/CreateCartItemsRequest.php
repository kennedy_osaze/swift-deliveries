<?php

namespace App\Http\Requests;

use App\Models\VendorOutlet;
use App\Traits\CanAddCartItemViaRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateCartItemsRequest extends FormRequest
{
    use CanAddCartItemViaRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cart' => 'required|array',
            'cart.*.quantity' => 'required|integer|min:1|max:1000',
            'cart.*.comment' => 'nullable|string|min_words:3|max:30000',
            'cart.*.item' => [
                'bail',
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if ($this->doesCartItemExistsInUserCart((string) $value)) {
                        $fail(trans('validation.cart_item_exists.multiple', compact('attribute')));
                    }
                },
                function ($attribute, $value, $fail) {
                    if (! $this->isItemAvailable((string) $value)) {
                        $fail(trans('validation.not_available.multiple', compact('attribute')));
                    }
                },
                function ($attribute, $value, $fail) {
                    if (! $this->itemIsOfSameOutlet((string) $value)) {
                        $fail(trans('validation.item_not_same_outlet.multiple', compact('attribute')));
                    }
                },
            ],
        ];
    }

    public function messages()
    {
        return [
            'cart.array' => 'The cart must contain the items to be added.',
            'cart.*.quantity.required' => 'An item must have a quantity.',
            'cart.*.quantity.min' => 'The quantity of an item must be at least :min',
            'cart.*.quantity.max' => 'The quantity of an item must not be more than :max',
            'cart.*.comment.min_words' => 'The comment on an item must contain at least :min words',
            'cart.*.comment.max' => 'The comment on an item must not be more than :max characters.',
            'cart.*.item.required' => 'A product item to add to cart is required',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function ($validator) {
            if (! $this->checkAllItemsAreUnique()) {
                $validator->errors()->add('cart', trans('validation.cart_items_unique'));
            }

            if (! $this->checkAllItemsAreOfSameOutlet()) {
                $validator->errors()->add('cart', trans('validation.cart_items_of_same_outlet'));
            }
        });

        return $validator;
    }

    /**
     * Get the validated data from the request.
     * It decodes the item to integer as another key
     *
     * @return \Illuminate\Support\Collection
     */
    public function validated()
    {
        $validated_data = parent::validated()['cart'];

        $cart_items = collect($validated_data)->map(function ($item) {
            $item['id'] = hash_id_decode($item['item']);
            return $item;
        });

        return $cart_items;
    }

    /**
     * This checks if all cart items are unique
     *
     * @return bool
     */
    private function checkAllItemsAreUnique()
    {
        $cart = $this->input('cart', []);

        return collect($cart)->unique('item')->count() === count($cart);
    }

    /**
     * This checks if all cart items belongs to the same vendor outlet
     *
     * @return bool
     */
    private function checkAllItemsAreOfSameOutlet()
    {
        $cart = $this->input('cart', []);

        $item_ids = collect($cart)->map(function ($item) {
            return hash_id_decode($item['item'] ?? '');
        })->filter()->toArray();

        $outlets_count = VendorOutlet::whereHas(
            'outletProductPivot',
            function ($query) use ($item_ids) {
                $query->whereIn('id', $item_ids);
            }
        )->count();

        return $outlets_count === 1;
    }
}
