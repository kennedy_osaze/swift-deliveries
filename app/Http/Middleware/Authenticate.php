<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('login');
        }

        if (! $request->is('admin') || $request->is('admin/*')) {
            return redirect()->guest('/admin/login');
        }

        if (! $request->is('vendor_admin') || $request->is('admin/vendor/*')) {
            return redirect()->guest('/admin/vendor/login');
        }

        if (! $request->is('outlet_admin') || $request->is('admin/outlet/*')) {
            return redirect()->guest('/admin/outlet/login');
        }
    }
}
