<?php

namespace App\Http\Middleware;

use Closure;

class IsOutletAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->outlet_id == null)
        {
            return redirect('/');
        }
        if(auth()->user()->vendor_id == null)
        {
            return redirect('/');
        }
        return $next($request);
    }
}
