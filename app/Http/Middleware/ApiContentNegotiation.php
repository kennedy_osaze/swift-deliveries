<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiContentNegotiation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Skip if request is not api bound
        if (! $request->is('api/*')) {
            return $next($request);
        }

        // Attach acceptable media type where not defined
        $request->headers->set('Accept', 'application/json');

        $response = $next($request);

        return $this->makeJsonResponse($response);
    }

    /**
     * Convert response to a JSON response
     *
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return Illuminate\Http\JsonResponse
     */
    private function makeJsonResponse($response)
    {
        if ($response instanceof JsonResponse) {
            return $response;
        }

        return response()->json(
            $response->content(),
            $response->status(),
            $response->headers->all(),
        );
    }
}
