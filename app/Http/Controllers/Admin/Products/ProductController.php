<?php

namespace App\Http\Controllers\Admin\Products;

use Carbon\Carbon;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Traits\UploadImageTrait;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    use UploadImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->paginate(15);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_categories = ProductCategory::all();
        return view('admin.products.create', compact('product_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        $this->validateRequest($request);
        
        // Check if a profile image has been uploaded
        if ($request->has('image')) {
            $path = '/products/';
            $image = $this->processImageRequest($request, $path);
        }

        Product::create([
            'name' => $request->name,
            'category_id' => $request->category_id,
            'description' => $request->description,
            'image' => $image,
            'active_at' => Carbon::now()
        ]);

        return redirect()->back()->with(['status' => 'Product added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id)->load('category');
        $product_categories = ProductCategory::all();
        return view('admin.products.edit', compact('product','product_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = null;

        // $this->validateRequest($request);

        $product = Product::find($id);
      
        if((bool)$request->is_available)
        {
            $product->active_at ? $product->active_at = null : $product->active_at = Carbon::now();

        } else
        {

            // Check if a profile image has been uploaded
            if ($request->has('image')) {
                $path = '/product-categories/';
                $image = $this->processImageRequest($request, $path);
            }

            $product->name = $request->name;
            $product->category_id = $request->category_id;
            $product->description = $request->description;
            $image ? ($product->image = $image) : '';

        }
       
        $product->save();

        return redirect()->back()->with(['status' => 'Product updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $product->deleted_at = Carbon::now();
        $product->save();

        return redirect()->back()->with(['status' => 'Product deleted successfully.']);
    }

    /**
     * Validate Product category request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:products|max:255',
            'category_id' => 'required'
        ]);
    }


}
