<?php

namespace App\Http\Controllers\Admin\Products;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Traits\UploadImageTrait;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    use UploadImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_categories = ProductCategory::all();
        return view('admin.product_categories.index', compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_types = ProductCategory::PRODUCT_TYPES;
        return view('admin.product_categories.create', compact('product_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        $this->validateRequest($request);
        
        // Check if a profile image has been uploaded
        if ($request->has('image')) {
            $path = '/product-categories/';
            $image = $this->processImageRequest($request, $path);
        }

        ProductCategory::create([
            'name' => $request->name,
            'product_type' => $request->product_type,
            'image' => $image,
            'active_at' => Carbon::now()
        ]);

        return redirect()->back()->with(['status' => 'Product category added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = ProductCategory::find($id);
        $product_types = ProductCategory::PRODUCT_TYPES;
        return view('admin.product_categories.edit', compact('product','product_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = null;

        // $this->validateRequest($request);

        $product = ProductCategory::find($id);
      
        if((bool)$request->is_available)
        {
            $product->active_at ? $product->active_at = null : $product->active_at = Carbon::now();

        } else
        {

            // Check if a profile image has been uploaded
            if ($request->has('image')) {
                $path = '/product-categories/';
                $image = $this->processImageRequest($request, $path);
            }

            $product->name = $request->name;
            $product->product_type = $request->product_type;
            $image ? ($product->image = $image) : '';

        }
       
        $product->save();

        return redirect()->back()->with(['status' => 'Product category updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = ProductCategory::find($id);

        $product->deleted_at = Carbon::now();
        $product->save();

        return redirect()->back()->with(['status' => 'Product category deleted successfully.']);
    }

    /**
     * Validate Product category request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:product_categories|max:255',
            'product_type' => 'required'
        ]);
    }


}
