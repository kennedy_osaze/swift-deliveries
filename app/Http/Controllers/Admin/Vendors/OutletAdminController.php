<?php

namespace App\Http\Controllers\Admin\Vendors;

use App\Models\Order;
use App\Models\CartItem;
use App\Models\VendorOutlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OutletAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = auth()->user()->outlet_id;

        $outlet = VendorOutlet::find($id);
       
        $paid_orders = Order::whereHas('cart.outletProducts', function ($query) use ($outlet) {
                $query->where('outlet_id', $outlet->id);
            })
            ->where(['status' => 'processing', 'payment_status' => 'paid'])
            ->with('cart.owner','cart.items')
            ->paginate(15);
           
        return view('admin.outlet_admins.index',compact('paid_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id)->load('cart.owner','cart.items');
        
        return view('admin.outlet_admins.show_details', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        $order->status == Order::STATUSES['PROCESSING'] ? $order->status =  Order::STATUSES['READY'] : Order::STATUSES['PROCESSING'];

        $order->save();

        return redirect()->back()->with(['status' => 'Order status updated and sent successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function processedOrders()
    {
        $id = auth()->user()->outlet_id;

        $outlet = VendorOutlet::find($id);
       
        $paid_orders = Order::whereHas('cart.outletProducts', function ($query) use ($outlet) {
                $query->where('outlet_id', $outlet->id);
            })
            ->where(['status' => 'delivered', 'status' => 'ready', 'payment_status' => 'paid'])
            ->with('cart.owner','cart.items')
            ->paginate(15);
           
        return view('admin.outlet_admins.processed',compact('paid_orders'));
    }
}
