<?php

namespace App\Http\Controllers\Admin\Vendors;

use Carbon\Carbon;
use App\Models\Vendor;
use App\Models\VendorAdmin;
use App\Models\VendorOutlet;
use Illuminate\Http\Request;
use App\Traits\UploadImageTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class VendorOutletController extends Controller
{
    use UploadImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendor_outlets = VendorOutlet::with('vendor')->paginate(15);
        return view('admin.vendor_outlets.index', compact('vendor_outlets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = Vendor::all();
        return view('admin.vendor_outlets.create', compact('vendors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validateRequest($request);
        
        DB::beginTransaction();
        $outlet = VendorOutlet::create([
            'vendor_id' => $request->vendor_id,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'active_at' => Carbon::now()
        ]);

        VendorAdmin::create([
            'vendor_id' => $request->vendor_id,
            'outlet_id' => $outlet->id,
            'username' => $request->username,
            'password' => Hash::make($request->password)
        ]);
        DB::commit();
        return redirect()->back()->with(['status' => 'Vendor outlet added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor_outlet = VendorOutlet::find($id)->load('admin');
        $vendors = Vendor::all();
        return view('admin.vendor_outlets.edit', compact('vendors','vendor_outlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor_outlet = VendorOutlet::find($id);
      
        if((bool)$request->is_available)
        {
            $vendor_outlet->active_at ? $vendor_outlet->active_at = null : $vendor_outlet->active_at = Carbon::now();

        } else
        {
            $vendor_outlet->vendor_id = $request->vendor_id;
            $vendor_outlet->latitude = $request->latitude;
            $vendor_outlet->longitude = $request->longitude;
            $vendor_outlet->address = $request->address;
            $vendor_outlet->city = $request->city;
            $vendor_outlet->state = $request->state;
            $vendor_outlet->vendor->username = $request->username;
            ($request->password) ? $vendor_outlet->vendor->password =$request->password : " ";
        }

        $vendor_outlet->vendor->save();
        $vendor_outlet->save();

        return redirect()->back()->with(['status' => 'Vendor outlet updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor_outlet = VendorOutlet::find($id);

        $vendor_outlet->deleted_at = Carbon::now();
        $vendor_outlet->save();

        return redirect()->back()->with(['status' => 'Vendor outlet deleted successfully.']);
    }

    /**
     * Validate Product category request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'vendor_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'address' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);
    }
}
