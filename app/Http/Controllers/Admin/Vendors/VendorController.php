<?php

namespace App\Http\Controllers\Admin\Vendors;

use Carbon\Carbon;
use App\Models\Vendor;
use App\Models\VendorAdmin;
use Illuminate\Http\Request;
use App\Traits\UploadImageTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class VendorController extends Controller
{
    use UploadImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendors = Vendor::paginate(15);
        return view('admin.vendors.index', compact('vendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Vendor::TYPES;
        return view('admin.vendors.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        $this->validateRequest($request);
        
        // Check if a profile image has been uploaded
        if ($request->has('image')) {
            $path = '/vendors/';
            $image = $this->processImageRequest($request, $path);
        }

        DB::beginTransaction();
        $vendor = Vendor::create([
            'name' => $request->name,
            'description' => $request->description,
            'vendor_type' => $request->vendor_type,
            'logo' => $image,
            'active_at' => Carbon::now()
        ]);

        VendorAdmin::create([
            'vendor_id' => $vendor->id,
            'username' => $request->username,
            'password' => Hash::make($request->password)
        ]);
        DB::commit();

        return redirect()->back()->with(['status' => 'Vendor added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::find($id);
        $types = Vendor::TYPES;
        return view('admin.vendors.edit', compact('types','vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image = null;

        // $this->validateRequest($request);

        $vendor = Vendor::find($id);
      
        if((bool)$request->is_available)
        {
            $vendor->active_at ? $vendor->active_at = null : $vendor->active_at = Carbon::now();

        } else
        {

            // Check if a profile image has been uploaded
            if ($request->has('image')) {
                $path = '/vendors/';
                $image = $this->processImageRequest($request, $path);
            }

            $vendor->name = $request->name;
            $vendor->description = $request->description;
            $vendor->vendor_type = $request->vendor_type;
            $vendor->vendorAdmin->username = $request->username;
            ($request->password) ? $vendor->vendorAdmin->password = $request->password : '';
            $image ? ($vendor->logo = $image) : '';

        }

        $vendor->vendorAdmin->save();
        $vendor->save();

        return redirect()->back()->with(['status' => 'Product updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::find($id);

        $vendor->deleted_at = Carbon::now();
        $vendor->save();

        return redirect()->back()->with(['status' => 'Product deleted successfully.']);
    }

    /**
     * Validate Product category request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:vendors|max:255',
            'vendor_type' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);
    }
}
