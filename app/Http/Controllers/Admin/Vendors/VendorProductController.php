<?php

namespace App\Http\Controllers\Admin\Vendors;

use Carbon\Carbon;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\VendorOutlet;
use Illuminate\Http\Request;
use App\Models\OutletProduct;
use App\Traits\UploadImageTrait;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\VendorProductImage;

class VendorProductController extends Controller
{
    use UploadImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendor_products = OutletProduct::with('product', 'outlet', 'outlet.vendor','outlet.vendor.productImages')->paginate(15);
        return view('admin.vendor_products.index', compact('vendor_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = Vendor::all();
        $outlets = VendorOutlet::all();
        $products = Product::all();
        return view('admin.vendor_products.create', compact('vendors','outlets','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image = null;

        $this->validateRequest($request);

        DB::beginTransaction();

            OutletProduct::create([
                'outlet_id' => $request->outlet_id,
                'product_id' => $request->product_id,
                'price' => $request->price,
                'active_at' => Carbon::now()
            ]);
            // Check if a profile image has been uploaded
            if ($request->has('image'))
            {
                $path = '/product-items/';
                $image = $this->processMultipleImageRequest($request, $path);
                foreach($image as $img)
                {

                    VendorProductImage::create([
                        'vendor_id' => $request->vendor_id,
                        'product_id' => $request->product_id,
                        'image' => $img
                    ]);
                }
            }else{
                VendorProductImage::create([
                    'vendor_id' => $request->vendor_id,
                    'product_id' => $request->product_id,
                    'image' => '/'
                ]);
            }
            
        DB::commit();
        return redirect()->back()->with(['status' => 'Product Item added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendors = Vendor::all();
        $outlets = VendorOutlet::all();
        $products = Product::all();
        $vendor_product = OutletProduct::find($id)->load('product', 'outlet', 'outlet.vendor','outlet.vendor.productImages');
        return view('admin.vendor_products.edit', compact('vendors','outlets','products','vendor_product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor_product = OutletProduct::find($id);
        // $vendor_product_image = VendorProductImage::where(['vendor_id' => $request->vendor_id, 'product_id' => $request->product_id])->first();

        $image = null;
      
        if((bool)$request->is_available)
        {
            $vendor_product->active_at ? $vendor_product->active_at = null : $vendor_product->active_at = Carbon::now();

        } else
        {
            $vendor_product->outlet_id = $request->outlet_id;
            $vendor_product->product_id = $request->product_id;
            $vendor_product->price = $request->price;


            // Check if a profile image has been uploaded
            // if ($request->has('image'))
            // {
            //     $path = '/product-items/';
            //     $image = $this->processMultipleImageRequest($request, $path);
            //     foreach($image as $img)
            //     {
            //         $vendor_product_image->vendor_id = $request->vendor_id;
            //         $vendor_product_image->product_id = $request->product_id;
            //         $vendor_product_image->image = $img;
            //         $vendor_product_image->save();
            //     }
            // }else{
            //     $vendor_product_image->vendor_id = $request->vendor_id;
            //         $vendor_product_image->product_id = $request->product_id;
            //         $vendor_product_image->save();
            // }
        }
       
        $vendor_product->save();

        return redirect()->back()->with(['status' => 'Vendor Product updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor_product = OutletProduct::find($id);

        $vendor_product->deleted_at = Carbon::now();
        $vendor_product->save();

        return redirect()->back()->with(['status' => 'Vendor product deleted successfully.']);
    }

    /**
     * Validate Product category request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'vendor_id' => 'required',
            'outlet_id' => 'required',
            'product_id' => 'required',
            'price' => 'required'
        ]);
    }
}
