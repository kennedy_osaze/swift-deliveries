<?php

namespace App\Http\Controllers\Admin\Dispatch;

use Carbon\Carbon;
use App\Models\Dispatch;
use App\Services\Tookan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DispatchTeam;
use Illuminate\Support\Facades\Hash;

class DispatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agents = Dispatch::paginate(15);
        return view('admin.dispatchs.index', compact('agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fleet_types = Dispatch::FLEET_TYPES;
        $transport_types = Dispatch::TRANSPORT_TYPES;
        $dispatch_teams = DispatchTeam::all();
        return view('admin.dispatchs.create', compact('fleet_types', 'transport_types','dispatch_teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);

        $request->request->add([
            'role_id' => 67696, 
            'color' => 'blue', 
            'transport_desc' => 'auto',
            'license' => 'demo',
            'timezone' => "-330"
            ]);

        try{
            $tookan = new Tookan();
            $tookan_request = $tookan->unsetTokenFromRequest($request);
            $tookan_request->merge(['fleet_type' => (int) $tookan_request->fleet_type])->all();
            $agent = $tookan->addAgent($tookan_request->all());
            $data = json_decode($agent['data']);

            if($data->status != 200)
            {
                return redirect()->back()->withErrors($data->message);
            }

            Dispatch::create([
                'firstname' => $request->first_name,
                'lastname' => $request->last_name,
                'username' => $request->username,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->passoword),
                'transport_type' => $request->transport_type,
                'team_id' => $request->team_id,
                'role_id' => $request->role_id,
                'fleet_type' => $request->fleet_type ? $request->fleet_type : Dispatch::FLEET_TYPES['CAPTIVE']
            ]);

        }catch(\Exception $e)
        {
            return redirect()->back()->withErrors($e);
        }

        return redirect()->back()->with(['status' => 'Dispatch Agent Added Successfully!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Dispatch::find($id);
        $fleet_types = Dispatch::FLEET_TYPES;
        $transport_types = Dispatch::TRANSPORT_TYPES;
        $dispatch_teams = DispatchTeam::all();
        return view('admin.dispatchs.edit', compact('agent','fleet_types','transport_types','dispatch_teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateRequest($request); 

        $agent = Dispatch::find($id);

        $agent->firstname = $request->first_name;
        $agent->lastname = $request->last_name;
        $agent->username = $request->username;
        $agent->email = $request->email;
        $agent->phone = $request->phone;
        $agent->transport_type = $request->transport_type;
        $agent->fleet_type = $request->fleet_type;
        ($request->password )? $agent->password = Hash::make($request->password) : '' ;

        $agent->save();

        return redirect()->back()->with(['status' => 'Dispatch Agent Updated Successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agent = Dispatch::find($id);

        $agent->deleted_at = Carbon::now();
        $agent->save();

        return redirect()->back()->with(['status' => 'Agent deleted successfully.']);
    }

    /**
     * Validate dispatch agent request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:dispatches,username,'.$request->id,
            'email' => 'required|max:255|unique:dispatches,email,'.$request->id,
            'phone' => 'required|unique:dispatches,phone,'.$request->id,
            'transport_type' => 'required'
        ]);
    }
}
