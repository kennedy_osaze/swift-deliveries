<?php

namespace App\Http\Controllers\Admin\Dispatch;

use Carbon\Carbon;
use App\Services\Tookan;
use App\Models\DispatchTeam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = DispatchTeam::paginate(15);
        $battery_usages = DispatchTeam::BATTERY_USAGE;

        return view('admin.dispatch_teams.index', compact('teams','battery_usages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $battery_usages = DispatchTeam::BATTERY_USAGE;

        return view('admin.dispatch_teams.create', compact('battery_usages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateRequest($request);
        
        try{

            $tookan = new Tookan();
            $tookan_request = $tookan->unsetTokenFromRequest($request);

            $team = $tookan->createTeam($tookan_request->all());
            $data = json_decode($team['data']);

            if($data->status != 200)
            {
                return redirect()->back()->withErrors($data->message);
            }

            DispatchTeam::create([
                'name' => $request->team_name,
                'battery_usage' => $request->battery_usage,
                'tags' => $request->tags,
                'team_id' => $data->data->team_id
            ]);

        }catch(\Exception $e)
        {
            return redirect()->back()->withErrors($e);
        }

        return redirect()->back()->with(['status' => 'Dispatch team has been created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $battery_usages = DispatchTeam::BATTERY_USAGE;

        $team = DispatchTeam::find($id);

        return view('admin.dispatch_teams.edit', compact('battery_usages', 'team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateRequest($request);

        $team = DispatchTeam::find($id);

        try
        {

            $tookan = new Tookan();
            $tookan_request = $tookan->unsetTokenFromRequest($request);
            $tookan_request->request->add(['team_id' => $team->team_id]);
           
            $response = $tookan->updateTeam($tookan_request->all());
            $data = json_decode($response['data']);

            if($data->status != 200)
            {
                return redirect()->back()->withErrors($data->message);
            }

            $team->name = $request->team_name;
            $team->battery_usage = $request->battery_usage;
            $team->tags = $request->tags;

            $team->save();

        }
        catch(\Exception $e)
        {
            return redirect()->back()->withErrors($e);
        }

        return redirect()->back()->with(['status' => 'Dispatch team has been updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = DispatchTeam::find($id);

        try
        {
            $tookan = new Tookan();

            $response = $tookan->deleteTeam(['team_id' => $team->team_id]);
            $data = json_decode($response['data']);

            if($data->status != 200)
            {
                return redirect()->back()->withErrors($data->message);
            }

        }catch(\Exception $e)
        {
            return redirect()->back()->withErrors($e);
        }

        $team->deleted_at = Carbon::now();
        $team->save();

        return redirect()->back()->with(['status' => 'Dispatch team has been deleted successfully']);
    }

    /**
     * Validate dispatch team request
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    private function validateRequest(Request $request)
    {
        $request->validate([
            'team_name' => 'required|unique:dispatch_teams,name,'.$request->id,
            'team_id' => 'unique:dispatch_teams,team_id,'.$request->id,
            'battery_usage' => 'required',
            'tags' => 'required',
        ]);
    }
}
