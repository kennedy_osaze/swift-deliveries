<?php

namespace App\Http\Controllers\Api\Product;

use App\Models\Vendor;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\OutletProduct;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Auth;
use App\Filters\OutletProductFilter;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\Vendor as VendorResource;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCategory as CategoryResource;
use App\Http\Resources\OutletProduct as OutletProductResource;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $per_page = (int) $request->query('per_page', 100);

        $categories = ProductCategory::available()
            ->withCountOfAvailableItems()
            ->withProductDetails()
            ->withCountAvailableVendors()
            ->paginate($per_page);

        return CategoryResource::collection($categories);
    }

    /**
     * Display the specified resource.
     *
     * @param string $category
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(string $category)
    {
        $category = ProductCategory::whereHashIdOrSlug($category)
            ->available()
            ->withCountOfAvailableItems()
            ->withProductDetails()
            ->withAvailableVendors()
            ->firstOrFail();

        return new CategoryResource($category);
    }

    public function showVendors(string $category)
    {
        $category = $this->findAvailableCategory($category);
        $per_page = (int) request('per_page', 100);

        $vendors = Vendor::availableAsCategory($category)
            ->withCountOfAvailableItems($category)
            ->withOutletDetails($category)
            ->withCountAvailableCategories()
            ->paginate($per_page);

        return VendorResource::collection($vendors);
    }

    public function showProducts(string $category)
    {
        $category = $this->findAvailableCategory($category);
        $per_page = (int) request('per_page', 100);

        $products = Product::availableAsCategory($category)
            ->withProductDetails()
            ->paginate($per_page);

        return ProductResource::collection($products);
    }

    public function showProductItems(string $category, OutletProductFilter $filter)
    {
        $category = $this->findAvailableCategory($category);
        $per_page = (int) request('per_page', 100);

        $product_items = OutletProduct::availableAsCategory($category)
            ->with('outlet.vendor')
            ->withProductDetails()
            ->withRatings()
            ->withFavouritesCount(Auth::user())
            ->filter($filter)
            ->paginate($per_page);

        return OutletProductResource::collection($product_items);
    }

    /**
     * Finds a product category by the hash_id or slug provided
     *
     * @return \App\Models\ProductCategory
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    private function findAvailableCategory(string $category)
    {
        return ProductCategory::whereHashIdOrSlug($category)
            ->available()
            ->firstOrFail();
    }
}
