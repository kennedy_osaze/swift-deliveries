<?php

namespace App\Http\Controllers\Api\Product;

use Illuminate\Http\Request;
use App\Models\OutletProduct;
use Illuminate\Support\Facades\Auth;
use App\Filters\OutletProductFilter;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\OutletProduct as OutletProductResource;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, OutletProductFilter $filter)
    {
        $per_page = (int) $request->query('per_page', 100);

        $product_items = OutletProduct::available()
            ->with('outlet.vendor')
            ->withProductDetails()
            ->withRatings()
            ->withFavouritesCount(Auth::user())
            ->filter($filter)
            ->paginate($per_page);

        return OutletProductResource::collection($product_items);
    }

    /**
     * Display the specified resource.
     *
     * @param string $product_item
     *
     * @return \Illuminate\Http\Response
     */
    public function show(string $item)
    {
        $product_item = OutletProduct::whereHashIdOrSlug($item)
            ->available()
            ->with('outlet.vendor')
            ->withProductDetails()
            ->withRatings()
            ->withFavouritesCount(Auth::user())
            ->firstOrFail();

        return new OutletProductResource($product_item);
    }
}
