<?php

namespace App\Http\Controllers\Api\Product;

use Illuminate\Http\Request;
use App\Models\OutletProduct;
use Illuminate\Support\Facades\Auth;
use App\Filters\OutletProductFilter;
use App\Builders\OutletProductBuilder;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\OutletProduct as OutletProductResource;

class ItemSearchController extends Controller
{
    /**
     * Searches for the product items a term
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request, OutletProductFilter $filter)
    {
        $per_page = (int) $request->query('per_page', 100);

        $product_items = OutletProduct::search($request->q)
            ->query(function (OutletProductBuilder $query) use ($filter) {
                $query->available()
                ->with('outlet.vendor')
                ->withProductDetails()
                ->withRatings()
                ->withFavouritesCount(Auth::user())
                ->filter($filter);
            })
            ->paginate($per_page);

        return OutletProductResource::collection($product_items);
    }
}
