<?php

namespace App\Http\Controllers\Api\Product;

use App\Models\Vendor;
use App\Models\Product;
use App\Models\OutletProduct;
use Illuminate\Support\Facades\Auth;
use App\Filters\OutletProductFilter;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\Vendor as VendorResource;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\OutletProduct as OutletProductResource;

class ProductController extends Controller
{
    public function index()
    {
        $per_page = (int) request('per_page', 100);

        $products = Product::available()
            ->withProductDetails()
            ->paginate($per_page);

        return ProductResource::collection($products);
    }

    public function show(string $product)
    {
        $product = Product::whereHashIdOrSlug($product)
            ->withProductDetails()
            ->firstOrFail();

        return new ProductResource($product);
    }

    public function showProductItems(string $product, OutletProductFilter $filter)
    {
        $product = $this->findAvailableProduct($product);
        $per_page = (int) request('per_page', 100);

        $product_items = OutletProduct::whereHasProductIsAvailable($product)
            ->withProductDetails()
            ->withRatings()
            ->withFavouritesCount(Auth::user())
            ->filter($filter)
            ->paginate($per_page);

        return OutletProductResource::collection($product_items);
    }

    public function showVendors(string $product)
    {
        $product = $this->findAvailableProduct($product);
        $per_page = (int) request('per_page', 100);

        $vendors = Vendor::whereHasProductIsAvailable($product)
            ->withCountOfAvailableProductItems($product)
            ->withOutletsDetailsForProduct($product)
            ->withAvailableCategories()
            ->paginate($per_page);

        return VendorResource::collection($vendors);
    }

    /**
     * Finds a product by the hash_id or slug provided
     *
     * @return \App\Models\Product
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    private function findAvailableProduct(string $product)
    {
        return Product::whereHashIdOrSlug($product)
            ->available()
            ->firstOrFail();
    }
}
