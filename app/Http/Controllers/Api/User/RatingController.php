<?php

namespace App\Http\Controllers\Api\User;

use App\Models\UserRating;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserRating  $userRating
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRating $userRating)
    {
        //
    }
}
