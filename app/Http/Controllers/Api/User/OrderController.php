<?php

namespace App\Http\Controllers\Api\User;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\Order as OrderResource;

class OrderController extends Controller
{
    public function index()
    {
        $per_page = (int) request('per_page', 100);
        $orders = Auth::user()->orders()->latest()->paginate($per_page);

        return OrderResource::collection($orders);
    }

    public function show(string $order)
    {
        $order = $this->findOrder($order);

        return new OrderResource($order);
    }

    public function cancel(string $order)
    {
        $order = $this->findOrder($order);

        if ($order->status !== Order::STATUSES['PENDING']) {
            return $this->forbiddenResponse(
                'This order cannot be cancelled at this stage.'
            );
        }

        $order->update(['status' => Order::STATUSES['CANCELLED']]);

        return $this->okResponse('The order has been cancelled successfully.');
    }

    /**
     * Finds an order by the hash_id provided
     *
     * @return \App\Models\Order
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    private function findOrder(string $order)
    {
        return Auth::user()->orders()
            ->whereHashId($order)
            ->firstOrFail();
    }
}
