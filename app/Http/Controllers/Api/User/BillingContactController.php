<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Models\BillingContact;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;
use Illuminate\Support\Facades\Validator;

class BillingContactController extends Controller
{
    public function index()
    {
        $per_page = (int) request('per_page', 100);
        $billing_contacts = Auth::user()->billingContacts()->paginate($per_page);

        $billing_contacts->getCollection()
            ->transform(function ($contact) {
                return $contact->getDetails();
            });

        return $this->okResponse(
            'The billing contacts for user has been retrieved successfully',
            $billing_contacts->toArray(),
        );
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $no_contact_yet = BillingContact::doesntExist();

        $contact = Auth::user()->billingContacts()->create(
            $request->only(['address', 'longitude', 'latitude'])
        );

        if ($no_contact_yet || $request->active) {
            BillingContact::setContactAsActive($contact);
        }

        return $this->createdResponse(
            'The billing contact has been created successfully.',
            $contact->getDetails()
        );
    }

    public function show(string $contact)
    {
        $billing_contact = $this->findBillingContactByHashId($contact);

        return $this->okResponse(
            'The billing contact has been retrieved successfully.',
            $billing_contact->getDetails(),
        );
    }

    public function update(Request $request, string $contact)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $billing_contact = $this->findBillingContactByHashId($contact);

        $billing_contact->update(
            $request->only(['address', 'longitude', 'latitude'])
        );

        if ($request->active) {
            BillingContact::setContactAsActive($billing_contact);
        }

        return $this->okResponse(
            'The billing contact has been updated successfully',
            $billing_contact->getDetails()
        );
    }

    public function destroy(string $contact)
    {
        $billing_contact = $this->findBillingContactByHashId($contact);

        $billing_contact->delete();

        return $this->noContentResponse();
    }

    /**
     * Get the validator for BillContact Model
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function validator(array $data)
    {
        return Validator::make($data, [
            'address' => 'required|string|min_words:3|max:190',
            'longitude' => 'nullable|numeric|min:-180|max:180',
            'latitude' => 'nullable|numeric|min:-90|max:90',
            'active' => 'nullable|boolean',
        ]);
    }

    /**
     * Finds a billing contact for the current user by the hash id provided
     *
     * @param string $hash_id
     *
     * @return \App\Models\BillingContact
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    private function findBillingContactByHashId(string $hash_id)
    {
        return Auth::user()->billingContacts()
            ->where('id', hash_id_decode($hash_id))
            ->firstOrFail();
    }
}
