<?php

namespace App\Http\Controllers\Api\User;

use App\Models\OutletProduct;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\OutletProduct as OutletProductResource;

class FavouriteController extends Controller
{
    /**
     * Display product items added as favourites
     * by authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function showProductItems()
    {
        $per_page = (int) request('per_page', 100);

        $favourite_items = OutletProduct::available()
            ->whereIsUserFavourites(Auth::user())
            ->with('outlet.vendor')
            ->withProductDetails()
            ->withRatings()
            ->paginate($per_page);

        return OutletProductResource::collection($favourite_items);
    }

    /**
     * Save a product item as a favourite for the user
     *
     * @param string $item
     *
     * @return \Illuminate\Http\Response
     */
    public function store(string $item)
    {
        $product_item = OutletProduct::whereHashIdOrSlug($item)
            ->available()
            ->firstOrFail();

        if ($product_item->isFavouredBy($user = Auth::user())) {
            return $this->badRequestResponse(
                'This item has already been added as a favourite'
            );
        }

        $product_item->addAsUserFavourite($user);

        return $this->okResponse(
            'The product item has been added as a favourite successfully'
        );
    }

    /**
     * Removes a product item from the user's favourites list
     *
     * @param string $item
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $item)
    {
        $product_item = OutletProduct::whereHashIdOrSlug($item)
            ->available()
            ->firstOrFail();

        if (! $product_item->isFavouredBy($user = Auth::user())) {
            return $this->badRequestResponse(
                'The item was never in the user\'s items favourite'
            );
        }

        $product_item->removeUserFavourite($user);

        return $this->okResponse(
            'The item has been removed from the user\'s items favourites'
        );
    }
}
