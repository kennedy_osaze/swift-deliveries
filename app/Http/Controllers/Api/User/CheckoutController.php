<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;

class CheckoutController extends Controller
{
    public function checkout()
    {
        $cart = Auth::user()->activeCart;

        if (! $cart || $cart->items()->doesntExist()) {
            return $this->badRequestResponse('No item is in the cart currently.');
        }

        $order = $cart->createOrder();

        return $this->createdResponse(
            'An order has been created to successfully.',
            $order->getDetails()
        );
    }
}
