<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;

class PaymentCardController extends Controller
{
    public function destroy(string $card)
    {
        $card = Auth::user()->paymentCards()
            ->where('id', hash_id_decode($card))
            ->firstOrFail();

        // Check if the card is currently used for uncompleted order
        if ($card->orders()->whereNotCompleted()->exists()) {
            return $this->badRequestResponse(
                'The card is currently been used to process a card.'
            );
        }

        $card->delete();

        return $this->noContentResponse();
    }
}
