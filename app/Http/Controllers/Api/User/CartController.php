<?php

namespace App\Http\Controllers\Api\User;

use App\Models\Cart;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;
use App\Http\Requests\SaveCartItemRequest;
use App\Http\Requests\CreateCartItemsRequest;
use App\Http\Resources\CartItem as CartItemResource;

class CartController extends Controller
{
    public function index()
    {
        $cart = $this->getUserActiveCart();
        $per_page = (int) request('per_page', 100);

        $cart_items = $cart->items()
            ->with(['outletProduct' => function ($query) {
                $query->with('outlet.vendor', 'product');
            }])
            ->paginate($per_page);

        return CartItemResource::collection($cart_items);
    }

    public function store(SaveCartItemRequest $request)
    {
        $added_item = Cart::addItem(Auth::user(), $request->validated());

        return $this->createdResponse(
            'The item was added successfully.',
            $added_item->getBasicDetails()
        );
    }

    public function storeMultiple(CreateCartItemsRequest $request)
    {
        $cart = Cart::addMultipleItems(Auth::user(), $request->validated());

        return $this->createdResponse(
            'The items have been added to cart successfully.',
            ['cart' => hash_id_encode($cart->id)]
        );
    }

    public function destroy()
    {
        $cart = $this->getUserActiveCart();

        $cart->items()->delete();

        return $this->noContentResponse();
    }

    public function showItem(string $item)
    {
        $cart = $this->getUserActiveCart();
        $cart_item = $this->findCartItemByHash($cart, $item);

        return new CartItemResource($cart_item);
    }

    public function updateItem(SaveCartItemRequest $request, string $item)
    {
        $cart = $this->getUserActiveCart();
        $cart_item = $this->findCartItemByHash($cart, $item);

        $cart_item->update(
            Arr::only($request->validated(), ['quantity', 'comment'])
        );

        return $this->okResponse(
            'The cart item has been updated successfully.',
            $cart_item->getBasicDetails()
        );
    }

    public function deleteItem($item)
    {
        $cart = $this->getUserActiveCart();
        $cart_item = $this->findCartItemByHash($cart, $item);

        $cart_item->delete();

        return $this->noContentResponse();
    }

    /**
     * Gets the user's active cart
     *
     * @return \App\Models\Cart
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    private function getUserActiveCart()
    {
        $cart = Auth::user()->activeCart;

        abort_if(
            !$cart || $cart->items()->doesntExist(),
            404,
            'No cart item is currently available.'
        );

        return $cart;
    }

    /**
     * Finds a Cart Item by the hash id provided
     *
     * @param \App\Models\Cart $cart
     * @param string $hash_id
     *
     * @return \App\Models\CartItem
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    private function findCartItemByHash(Cart $cart, string $hash_id)
    {
        return $cart->items()
            ->whereHas('outletProduct', function ($query) use ($hash_id) {
                $query->whereHashId($hash_id);
            })
            ->firstOrFail();
    }
}
