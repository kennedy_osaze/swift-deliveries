<?php

namespace App\Http\Controllers\Api\Payment;

use App\Models\User;
use App\Models\Order;
use App\Services\Paystack;
use App\Models\PaymentCard;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Api\Controller;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{
    public function processOrderWithNewCard(string $order = null)
    {
        $pending_order = $this->findUserPendingOrderForPayment(
            $user = Auth::user(),
            $order
        );

        $this->checkOrderForBillingDetails($pending_order);

        $result = (new Paystack())->initializeTransaction(
            $user->email,
            $pending_order->total_amount,
            $pending_order->payment_reference_code
        );

        Log::info(
            "Initialize payment transaction with Payment Provider for user:
            {$user->email} with order: {$pending_order->id} result",
            $result
        );

        if ($result['status'] !== 200 || ! data_get($result, 'data.status', false)) {
            if ($result['status'] === 400) {
                abort_with_error(400, [
                    'code' => 'payment_error',
                    'message' => data_get($result, 'data.message')
                ], 'Unable to initialize transaction.');
            }

            return $this->serverErrorResponse(
                'The payment service is not available at the moment.'
            );
        }

        $pending_order->update([
            'status' => Order::STATUSES['PROCESSING'],
            'payment_status' => Order::PAYMENT_STATUSES['PENDING']
        ]);

        $result_data = $result['data']['data'];
        $response_data =
            [
                'order_id' => $pending_order->hashid(),
                'reference_code' => $pending_order->reference_code
            ]
            + Arr::only($result_data, ['authorization_url', 'access_code']);

        return $this->okResponse(
            'The order payment has been initiated successfully.',
            $response_data
        );
    }

    public function completeTransaction(string $reference)
    {
        $paystack = new Paystack();
        $result = $paystack->verifyTransaction($reference);

        Log::info(
            "Payment transaction reference verification for reference
            {$reference} result",
            $result
        );

        if ($result['status'] !== 200 || ! data_get($result, 'data.status')) {
            abort_with_error(400, [
                'code' => 'invalid_transaction_reference',
                'message' => data_get($result, 'data.message')
            ]);
        }

        $result_data = $result['data']['data'] ?? [];

        if (empty($result_data) || $result_data['status'] !== 'success') {
            abort_with_error(400, [
                'code' => 'transaction_failed',
                'message' => data_get($result_data, 'data.message')
            ]);
        }

        $processing_order = Order::wherePaymentReference($result_data['reference'] ?? '')
            ->whereAwaitingPaymentVerification()
            ->first();

        if (! $processing_order) {
            $this->invalidateUnavailableOrder();
        }

        $processing_order->completePayment($result_data);

        return $this->okResponse('The transaction has been completed successfully.');
    }

    public function payOrderWithSavedCard(Request $request, string $order = null)
    {
        $validator = $this->paymentCardValidator($request->all());

        if ($validator->fails()) {
            return $this->validationErrorResponse($validator);
        }

        $pending_order = $this->findUserPendingOrderForPayment(
            $user = Auth::user(),
            $order
        );

        $payment_card = PaymentCard::findByHashid($request->payment_card);

        if (! $payment_card->is_reusable) {
            abort_with_error(400, ['code' => 'card_not_reusable']);
        }

        $result = (new Paystack())->chargeAuthorization(
            $user->email,
            $pending_order->total_amount,
            $pending_order->payment_reference_code,
            $payment_card->authorization_code
        );

        Log::info(
            "Charge card authorization with Payment Provider for user:
            {$user->email} with order: {$pending_order->id}",
            $result
        );

        if ($result['status'] !== 200 || data_get($result['data'], 'data.status', false)) {
            if ($result['status'] === 400) {
                abort_with_error(400, [
                    'code' => 'payment_error',
                    'message' => data_get($result, 'data.message'),
                ]);
            }

            return $this->serverErrorResponse(
                'The payment service is not available at the moment.'
            );
        }

        $pending_order->updateAfterPaymentMadeWithCard($payment_card);

        return $this->okResponse(
            'Order payment was successful.'
        );
    }

    /**
     * Finds a user latest pending order that is available for payment
     *
     * @param \App\Models\User $user
     * @param string|null $order
     *
     * @return \App\Models\Order
     *
     * @throws \App\Exceptions\HttpExceptionWithErrorData
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function findUserPendingOrderForPayment(User $user, string $order = null)
    {
        $pending_order = Order::latestPendingForPaymentByUser($user, $order)
            ->first();

        if (! $pending_order) {
            $this->invalidateUnavailableOrder($order);
        }

        return $pending_order;
    }

    /**
     * Invalidates an order that is not available for payment
     *
     * @return void
     *
     * @throws \App\Exceptions\HttpExceptionWithErrorData
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function invalidateUnavailableOrder(string $order = null)
    {
        if (! $order) {
            abort_with_error(400, ['code' => 'payment_unavailable_order']);
        }

        abort_if(! Order::findByHashid($order), 404, 'This order does not exist.');

        abort_with_error(400, ['code' => 'order_not_payable']);
    }

    /**
     * Checks if the order has a billing contact tied to and tries to
     * update the order where there is an active contact
     *
     * @param \App\Models\Order
     *
     * @return bool
     *
     * @throws \App\Exceptions\HttpExceptionWithErrorData
     */
    private function checkOrderForBillingDetails(Order $order)
    {
        if ($order->contact_id) {
            return true;
        }

        if (! $billing_contact = Auth::user()->activeBillingContact) {
            abort_with_error(400, ['code' => 'billing_contact_required']);
        }

        return $order->update(['contact_id' => $billing_contact->id]);
    }

    /**
     * Get the validator for validating payment card id
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function paymentCardValidator(array $data)
    {
        return Validator::make($data, [
            'payment_card' => ['required', function ($attribute, $value, $fail) {
                $is_valid_card = Auth::user()->paymentCards()
                    ->where('id', hash_id_decode($value))
                    ->whereNotNull('authorization_code')
                    ->exists();

                if (! $is_valid_card) {
                    $fail($attribute . ' is invalid.');
                }
            }],
        ]);
    }
}
