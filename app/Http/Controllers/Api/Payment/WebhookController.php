<?php

namespace App\Http\Controllers\Api\Payment;

use App\Models\Order;
use App\Services\Paystack;
use App\Http\Controllers\Api\Controller;

class WebhookController extends Controller
{
    public function handle()
    {
        $paystack = new Paystack;
        $result = $paystack->getWebhookData();

        if (empty($result)) {
            return $this->okResponse('Success.');
        }

        if ($result['event'] === 'charge.success') {
            $this->processTransactionWebhook($result['data'] ?? []);
        }

        return $this->okResponse('Success.');
    }

    /**
     * Handle transaction webhook for the event (charge.success)
     *
     * @param array $data The webhook data
     *
     * @return null;
     *
     * @throws \Exception
     */
    private function processTransactionWebhook(array $data)
    {
        if (data_get($data, 'status') !== 'success') {
            return;
        }

        $processing_order = Order::wherePaymentReference($data['reference'] ?? '')
            ->whereAwaitingPaymentVerification()
            ->first();

        if (! $processing_order) {
            return;
        }

        $processing_order->completePayment($data);
    }
}
