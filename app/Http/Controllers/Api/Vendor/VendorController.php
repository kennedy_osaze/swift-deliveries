<?php

namespace App\Http\Controllers\Api\Vendor;

use App\Models\Vendor;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\OutletProduct;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Auth;
use App\Filters\OutletProductFilter;
use App\Http\Controllers\Api\Controller;
use App\Http\Resources\Vendor as VendorResource;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\OutletProduct as OutletProductResource;
use App\Http\Resources\ProductCategory as ProductCategoryResource;

class VendorController extends Controller
{
    public function index(Request $request)
    {
        $per_page = (int) $request->query('per_page', 100);

        $vendors = Vendor::available()
            ->withCountOfAvailableItems()
            ->withOutletDetails()
            ->withCountAvailableCategories()
            ->paginate($per_page);

        return VendorResource::collection($vendors);
    }

    public function show(string $vendor)
    {
        $vendor = Vendor::whereHashIdOrSlug($vendor)
            ->available()
            ->withCountOfAvailableItems()
            ->withOutletDetails()
            ->withAvailableCategories()
            ->firstOrFail();

        return new VendorResource($vendor);
    }

    public function showProducts(string $vendor)
    {
        $vendor = $this->findAvailableVendor($vendor);
        $per_page = (int) request('per_page', 100);

        $products = Product::availableAsVendor($vendor)
            ->with('category')
            ->withRatingsCount($vendor)
            ->withAverageRatings($vendor)
            ->withAnyVendorItemImageAvailable($vendor)
            ->withAvailableOutletsAndItemsCounts($vendor)
            ->paginate($per_page);

        return ProductResource::collection($products);
    }

    public function showProductItems(string $vendor, OutletProductFilter $filter)
    {
        $vendor = $this->findAvailableVendor($vendor);
        $per_page = (int) request('per_page', 100);

        $product_items = OutletProduct::availableAsVendor($vendor)
            ->with('outlet.vendor')
            ->withProductDetails()
            ->withRatings()
            ->withFavouritesCount(Auth::user())
            ->filter($filter)
            ->paginate($per_page);

        return OutletProductResource::collection($product_items);
    }

    public function showProductCategories(string $vendor)
    {
        $vendor = $this->findAvailableVendor($vendor);
        $per_page = (int) request('per_page', 100);

        $product_categories = ProductCategory::availableAsVendor($vendor)
            ->withCountOfAvailableItems($vendor)
            ->withProductDetails()
            ->paginate($per_page);

        return ProductCategoryResource::collection($product_categories);
    }

    /**
     * Finds a vendors by the hash_id or slug provided
     *
     * @return \App\Models\Vendor
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    private function findAvailableVendor(string $vendor)
    {
        return Vendor::whereHashIdOrSlug($vendor)
            ->available()
            ->firstOrFail();
    }
}
