<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Api\Controller;

class ForgotPasswordController extends Controller
{
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        $link_sent = $response == (
            Password::RESET_LINK_SENT
            || config('auth.passwords.misinform_user_with_invalid_credentials', false)
        );

        return $link_sent
            ? $this->okResponse('Password reset link has been sent to the email provided.')
            : $this->badRequestResponse('The email provided is not valid.');
    }
}
