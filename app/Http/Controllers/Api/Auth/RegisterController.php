<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Notifications\VerifyEmail;
use App\Http\Controllers\Api\Controller;
use App\Http\Requests\CreateUserRequest;

class RegisterController extends Controller
{
    /**
     * Handle the registration request.
     *
     * @param  \App\Http\Requests\CreateUserRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(CreateUserRequest $request)
    {
        $user = $this->createUser($request->validated());

        $user->notify(new VerifyEmail);

        return $this->createdResponse('User registered successfully.', $user->toArray());
    }

    /**
     * Creates and returns a user
     *
     * @param array $data
     *
     * @return \App\Models\User
     */
    private function createUser(array $data)
    {
        return User::create([
            'first_name' => ucwords($data['first_name']),
            'last_name' => ucwords($data['last_name']),
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
