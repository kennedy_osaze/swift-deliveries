<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\Controller;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('logout');
    }

    /**
     * Handle a login request to the API.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (! $user = $this->attemptLogin($this->credentials($request))) {
            return $this->unauthenticatedResponse('Invalid login credentials.');
        }

        $token_name = sprintf("%s-mobile-%s", $user->email, now()->timestamp);
        $access_token = $user->createToken($token_name)->accessToken;

        return $this->okResponse('User logged in successfully.', [
            'token' => $access_token,
            'user' => $user->toArray(),
        ]);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    private function validateLogin(Request $request)
    {
        $request->validate([
            'email_phone' => 'required|string',
            'password' => 'required|string',
        ], [
            'email_phone.required' => 'The email_phone field is required',
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    private function credentials(Request $request)
    {
        $login_type = filter_var($request->email_phone, FILTER_VALIDATE_EMAIL)
            ? 'email' : 'phone';

        $request->merge([$login_type => $request->input('email_phone')]);

        return $request->only([$login_type, 'password']);
    }

    /**
     * Attempt to log the user.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \App\Models\User|null
     */
    private function attemptLogin(array $credentials)
    {
        $password = $credentials['password'];
        unset($credentials['password']);

        if (! $user = User::where($credentials)->first()) {
            return null;
        }

        return Hash::check($password, $user->password) ? $user : null;
    }

    /**
     * Log the user out.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();

        $token->revoke();

        return $this->okResponse('User logged out successfully.');
    }
}
