<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class Tookan extends BaseService
{
    /**
     * @inheritdoc
     */
    protected function baseUri()
    {
        return config('services.tookan.base_url');
    }

    /**
     * Create tookan agent
     *
     * @param array $request
     *
     * @return array
     */
    public function addAgent(array $request)
    {
        $request['api_key'] = config('services.tookan.keys.api_key');
        return $this->post('/v2/add_agent', ['json' => $request]);
    }

    /**
     * Create tookan agent
     *
     * @param array $request
     *
     * @return array
     */
    public function editAgent(array $request)
    {
        $request['api_key'] = config('services.tookan.keys.api_key');

        return $this->post('/v2/edit_agent', ['json' => $request]);
    }

    /**
     * Create tookan team
     *
     * @param array $request
     *
     * @return array
     */
    public function createTeam(array $request)
    {
        $request['api_key'] = config('services.tookan.keys.api_key');

        return $this->post('/v2/create_team', ['json' => $request]);
    }

    /**
     * Update tookan team
     *
     * @param array $request
     *
     * @return array
     */
    public function updateTeam(array $request)
    {
        $request['api_key'] = config('services.tookan.keys.api_key');

        return $this->post('/v2/update_team', ['json' => $request]);
    }

    /**
     * Delete tookan team
     *
     * @param array $request
     *
     * @return array
     */
    public function deleteTeam(array $request)
    {
        $request['api_key'] = config('services.tookan.keys.api_key');

        return $this->post('/v2/delete_team', ['json' => $request]);
    } 
    
    /**
     * unset token from request parameters 
     *
     * @param array $request
     *
     * @return array
     */
    public function unsetTokenFromRequest(Request $request)
    {
        unset($request['_token']);

        return $request;
    }
}
