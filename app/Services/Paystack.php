<?php

namespace App\Services;

use Illuminate\Support\Arr;

class Paystack extends BaseService
{
    /**
     * @inheritdoc
     */
    protected function baseUri()
    {
        return config('services.paystack.base_url');
    }

    /**
     * Initialize a user's very first transaction
     *
     * @param string $email
     * @param float amount
     * @param string $reference
     * @param string|null $callback_url
     * @param array|null $meta_data
     *
     * @return array
     */
    public function initializeTransaction(
        string $email, float $amount, string $reference, string $callback_url = null, array $meta_data = null
    ) {
        $request_data = [
            'email' => $email,
            'amount' => $this->formatAmount($amount),
            'reference' => $reference,
            'callback_url' => $callback_url ?? config('services.paystack.callback_url'),
        ];

        if (! empty($callback_url)) {
            $request_data['callback_url'] = $callback_url;
        }

        if (! empty($meta_data)) {
            $meta_data and $request_data['metadata'] = json_encode($meta_data);
        }

        return $this->post('/transaction/initialize', ['json' => $request_data]);
    }

    /**
     * Verifies the authenticity of a transaction
     *
     * @param string $reference
     *
     * @return $array
     */
    public function verifyTransaction(string $reference)
    {
        return $this->get("/transaction/verify/{$reference}");
    }

    /**
     * Charges a returning user (for subsequent card transactions)
     *
     * @param string $email
     * @param float amount
     * @param string $reference
     * @param string $auth_code
     * @param array|null $meta_data
     *
     * @return array
     */
    public function chargeAuthorization(
        string $email, float $amount, string $reference, string $auth_code, array $meta_data = null
    ) {
        $request_data = [
            'email' => $email,
            'amount' => $this->formatAmount($amount),
            'reference' => $reference,
            'authorization_code' => $auth_code,
        ];

        if (! empty($meta_data)) {
            $meta_data and $request_data['metadata'] = json_encode($meta_data);
        }

        return $this->post('/transaction/charge_authorization', ['json' => $request_data]);
    }

    /**
     * Handles and gets Paystack webhook request data
     *
     * @return array|null
     */
    public function getWebhookData()
    {
        if (strtoupper($_SERVER['REQUEST_METHOD']) !== 'POST') {
            return;
        }

        $request_raw_input = @file_get_contents("php://input");

        $signature = $_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] ?? '';
        $secret_key = config('services.paystack.keys.secret');

        if ($signature !== hash_hmac('sha512', $request_raw_input, $secret_key)) {
            return;
        }

        $webhook_result = json_decode($request_raw_input, true);

        if (! Arr::has($webhook_result, ['event', 'data'])) {
            return;
        }

        return $webhook_result;
    }

    /**
     * Deactivates a user's card
     *
     * @param string $auth_code
     *
     * @return array
     */
    public function deactivateAuthorization(string $auth_code)
    {
        $request_data = [
            'authorization_code' => $auth_code,
        ];

        return $this->post('/customer/deactivate_authorization', ['json' => $request_data]);
    }

    /**
     * This fetches the payment card details from the paystack result data
     * returned after successful transaction completion
     *
     * @param array $paystack_data
     *
     * @return array
     */
    public static function getPaymentCardDetails(array $paystack_data)
    {
        return [
            'email' => data_get($paystack_data, 'customer.email'),
            'authorization_code' => data_get($paystack_data, 'authorization.authorization_code'),
            'card_type' => data_get($paystack_data, 'authorization.card_type'),
            'last_four_digits' => data_get($paystack_data, 'authorization.last4'),
            'expiry_month' => data_get($paystack_data, 'authorization.exp_month'),
            'expiry_year' => data_get($paystack_data, 'authorization.exp_year'),
            'is_reusable' => data_get($paystack_data, 'authorization.reusable'),
        ];
    }

    /**
     * @inheritdoc
     */
    protected function clientConfig()
    {
        return array_merge(parent::clientConfig(), [
            'headers' => [
                'Authorization' => 'Bearer ' . config('services.paystack.keys.secret'),
                'Accept' => 'application/json',
            ],
        ]);
    }

    /**
     * Formats an amount as required by Paystack
     *
     * @param float $amount
     *
     * @return string
     */
    private function formatAmount(float $amount)
    {
        return (string) intval($amount * 100);
    }
}
