<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * @method ResponseInterface get(string|UriInterface $uri, array $options = [])
 * @method ResponseInterface head(string|UriInterface $uri, array $options = [])
 * @method ResponseInterface put(string|UriInterface $uri, array $options = [])
 * @method ResponseInterface post(string|UriInterface $uri, array $options = [])
 * @method ResponseInterface patch(string|UriInterface $uri, array $options = [])
 * @method ResponseInterface delete(string|UriInterface $uri, array $options = [])
 */
abstract class BaseService
{
    /** @var mixed */
    protected $response;

    /** @var \GuzzleHttp\Client Guzzle Client */
    protected $client = null;

    /** @var array The headers to use */
    protected $headers = [];

    /** @var int The HTTP response status code  */
    public $response_status = 200;

    /**
     * The base uri of the HTTP request
     *
     * @return string
     */
    abstract protected function baseUri();

    /**
     * Instantiate the Client class
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client(
            array_merge(['base_uri' => $this->normalizeBaseUri()],
            $this->clientConfig()
        ));
    }

    /**
     * Makes the request
     *
     * @param string $method
     * @param string $uri
     * @param array options See http://docs.guzzlephp.org/en/stable/request-options.html
     *
     * @return array The response of the request
     */
    public function makeRequest(string $method, string $uri, array $options = [])
    {
        $this->checkValidRequestMethod($method);

        if (! empty($this->headers)) {
            $options['headers'] = (! empty($options['headers']))
                ? array_merge($this->headers, $options['headers'])
                : $this->headers;
        }

        try {
            $this->response = $this->client->request(
                strtoupper($method),
                $this->normalizeUriPath($uri),
                $options
            );

            return $this->renderResponse();
        } catch (RequestException $e) {
            return $this->renderExceptionResponse($e);
        }
    }

    /**
     * Validates the request method
     *
     * @param string $method
     *
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    protected function checkValidRequestMethod(string $method)
    {
        if (! $this->isValidRequestMethod($method)) {
            throw new InvalidArgumentException("{$method} is not a valid request verb");
        }
    }

    /**
     * Check if the method provided is valid
     *
     * @param string $method
     *
     * @return bool
     */
    protected function isValidRequestMethod(string $method)
    {
        $valid_methods = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'PATCH'];

        return in_array(strtoupper($method), $valid_methods);
    }

    /**
     * Renders the result of the request made
     *
     * @return array
     */
    protected function renderResponse()
    {
        $this->response_status = $this->response->getStatusCode();

        return [
            'status' => $this->response_status,
            'data' => $this->getResponseData(),
        ];
    }

    /**
     * Get the response data from a request
     *
     * @return mixed
     */
    protected function getResponseData()
    {
        $response_data = $this->response->getBody()->getContents();

        if ($this->response->hasHeader('Content-Type')) {
            $header = $this->response->getHeader('Content-Type');

            $response_data = Str::startsWith($header[0], 'application/json')
                ? json_decode($response_data, true)
                : $response_data;
        }

        return $response_data;
    }

    /**
     * Renders the exception thrown from handling a request
     *
     * @param \GuzzleHttp\Exception\RequestException $exception
     *
     * @return array
     */
    public function renderExceptionResponse(RequestException $exception)
    {
        $this->response = $exception->hasResponse()
            ? $exception->getResponse()
            : null;

        $this->response_status = $this->response instanceof ResponseInterface
            ? $this->response->getStatusCode()
            : $exception->getCode();

        return [
            'status' => $this->response_status,
            'message' => $exception->getMessage(),
            'error' => $this->response instanceof ResponseInterface
                ? $this->getResponseData()
                : $exception->getMessage(),
        ];
    }

    /**
     * Get the client
     *
     * @return \GuzzleHttp\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Get the response
     *
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Defines the Client config
     * See http://docs.guzzlephp.org/en/stable/request-options.html
     *
     * @return array
     */
    protected function clientConfig()
    {
        return ['timeout' => 20];
    }

    /**
     * Set the header to be used for the request
     *
     * @param array $headers
     *
     * @return static
     */
    protected function setHeaders(array $headers)
    {
        $this->headers = array_merge($this->headers, $headers);

        return $this;
    }

    /**
     * Normalizes the base uri of the client request
     *
     * @return string;
     */
    protected function normalizeBaseUri()
    {
        return rtrim($this->baseUri(), '/');
    }

    /**
     * Normalizes the uri path of the client request
     *
     * @param string $path
     *
     * @return string
     */
    protected function normalizeUriPath(string $path)
    {
        return '/' . ltrim($path, '/');
    }

    /**
     * Dynamically pass parameters to make a request.
     *
     * @param string $method
     * @param array $parameters
     *
     * @return array
     */
    public function __call($method, $parameters)
    {
        $this->checkValidRequestMethod($method);

        return $this->makeRequest($method, ...$parameters);
    }
}
