<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $this->morphMapRelations();

        $this->extendValidator();
    }

    protected function morphMapRelations()
    {
        $morph_map = array_reduce($this->classesToMorphMap(), function ($carry, $class) {
            // Use the singular form of the class basename as key
            $key = Str::singular(Str::snake(class_basename($class)));

            return [$key => $class] + $carry;
        }, []);

        Relation::morphMap($morph_map);
    }

    protected function classesToMorphMap()
    {
        return [
            \App\Models\Vendor::class,
            \App\Models\OutletProduct::class,
            \App\Models\PaymentCard::class,
        ];
    }

    protected function extendValidator()
    {
        Validator::extend(
            'min_words',
            function ($attribute, $value, $parameters, $validator) {
                $value = preg_replace('/<.*?>/', '', $value);
                $length = $parameters[0];
                return count(preg_split('/\s+/u', $value, null, PREG_SPLIT_NO_EMPTY)) >= $length;
            }
        );

        Validator::replacer(
            'min_words',
            function ($message, $attribute, $rule, $parameters) {
                return strtr($message, [
                    ':attribute' => $attribute,
                    ':min' => $parameters[0],
                ]);
            }
        );
    }
}
