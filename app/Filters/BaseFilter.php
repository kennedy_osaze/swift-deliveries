<?php

namespace App\Filters;

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

abstract class BaseFilter
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $builder;

    /**
     * Create a filter instance
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters to the builder
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;

        foreach ($this->request->query() as $filter_name => $value) {
            $filter = Str::camel($filter_name);
            $value = $this->normalizeRequestFilterValue($value);

            if ($this->filterCanBeApplied($filter, $value)) {
                call_user_func([$this, $filter], $value);
            }
        }

        return $this->builder;
    }

    /**
     * Normalizes a request query filter value
     *
     * @param array|string $value
     *
     * @return mixed
     */
    protected function normalizeRequestFilterValue($value)
    {
        if (is_array($value) || Str::contains($value, ',')) {
            return collect(is_array($value) ? $value : explode(',', $value))
                ->map(function ($v) {
                    return $this->normalizeRequestFilterValue($v);
                })
                ->all();
        }

        $value = trim($value);

        if ($value === 'true') {
            return true;
        }

        if ($value === 'false') {
            return false;
        }

        if ($value === 'null') {
            return null;
        }

        return e($value);
    }

    /**
     * Checks if the filter can be applied with the given value
     *
     * @param string $filter
     * @param mixed $value
     *
     * @return boolean
     */
    protected function filterCanBeApplied(string $filter, $value)
    {
        if (! method_exists($this, $filter)) {
            return false;
        }

        $method = new \ReflectionMethod($this, $filter);

        if (! $method->isPublic()) {
            return false;
        }

        $parameter = Arr::first($method->getParameters());

        return $value
            ? $method->getNumberOfParameters() > 0
            : $parameter === null || $parameter->isDefaultValueAvailable();
    }

    /**
     * Sort the query by the sort field
     * sort field could be sort=-field1,field2 || sort= field || sort=field
     *
     * @param string $value
     * @param array $sort_map Maps field to the table column equivalents
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function sort(string $value, array $sort_map = [])
    {
        $model = $this->builder->getModel();

        collect(explode(',', $value))
            ->mapWithKeys(function (string $field) use ($sort_map) {
                $prefix = substr($field, 0, 1);

                if (in_array($prefix, ['-', ' '])) {
                    $field = substr($field, 1);

                    return [
                        $sort_map[$field] ?? $field
                        => $prefix === '-' ? 'desc': 'asc'
                    ];
                }

                return [$sort_map[$field] ?? $field => 'asc'];
            })
            ->each(function (string $order, string $field) use ($model) {
                $schema = $model->getConnection()->getSchemaBuilder();

                if ($schema->hasColumn($field, $model->getTable())) {
                    $this->builder->orderBy($field, $order);
                }
            });

        return $this;
    }
}
