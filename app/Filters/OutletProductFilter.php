<?php

namespace App\Filters;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class OutletProductFilter extends BaseFilter
{
    /**
     * Determines whether, when coordinate filter is used,
     * only the closest (vendor) outlet should be used when retrieving items
     *
     * @var bool
     */
    private $use_closest_outlet = false;

    /**
     * Implements the category filter
     *
     * Filters the product items by a category hash id, slug or an array of hash ids/slugs
     *
     * @param string|array $category
     *
     * @return void
     */
    public function category($category)
    {
        if (empty($category)) {
            return;
        }

        $categories = (is_string($category)) ? explode(',', $category) : $category;

        $this->builder->whereHas(
            'product.category',
            function ($query) use ($categories) {
                $query->where(function ($query) use ($categories) {
                    foreach ($categories as $category) {
                        $query->orWhere(function ($query) use ($category) {
                            $query->whereHashIdOrSlug(trim((string) $category));
                        });
                    }
                });
            }
        );
    }

    /**
     * Implements the vendor filter
     *
     * Filters the product item by a vendor hash id, slug or an array of hash ids/slugs
     *
     * @param string|array $vendor
     *
     * @return void
     */
    public function vendor($vendor)
    {
        if (empty($vendor)) {
            return;
        }

        $vendors = (is_string($vendor)) ? explode(',', $vendor) : $vendor;

        $this->builder->whereHas(
            'outlet.vendor',
            function ($query) use ($vendors) {
                $query->where(function ($query) use ($vendors) {
                    foreach ($vendors as $vendor) {
                        $query->orWhere(function ($query) use ($vendor) {
                            $query->whereHashIdOrSlug(trim((string) $vendor));
                        });
                    }
                });
            }
        );
    }

    /**
     * Implements the price filter
     *
     * Filters the outlet product by a specified price range
     * The price range must include the 'min' and 'max' value
     *
     * @param array $range
     *
     * @return void
     */
    public function price($range)
    {
        if (! is_array($range) || count($range) !== 2) {
            return;
        }

        if (! Arr::has($range, ['min', 'max'])) {
            return;
        }

        $this->builder->where(function ($query) use ($range) {
            $query->whereBetween(
                'price',
                [(float) $range['min'], (float) $range['max']]
            );
        });
    }

    /**
     * Implements the coordinate filter
     *
     * Filters the outlet product by a specified coordinate
     * The coordinate must include the 'longitude' and 'latitude' value
     *
     * @param array $coordinates
     *
     * @return void
     */
    public function coordinates($coordinates)
    {
        if (! is_array($coordinates)) {
            return;
        }

        $coordinates = collect($coordinates)->map(function ($value) {
            return floatval($value);
        });

        if (! $coordinates->has(['longitude', 'latitude'])) {
            return;
        }

        $this->builder->whereIn('outlet_id', function ($query) use ($coordinates) {
            $sub_query = $this->closestVendorOutletsPerVendorFromCoordinates(
                $coordinates['longitude'],
                $coordinates['latitude']
            );

            if ($this->shouldUseClosestVendorOutlet()) {
                $sub_query->take(1);
            }

            $query->fromSub($sub_query->select('id'), 'closest_outlets_by_distance');
        });
    }

    /**
     * This forces the coordinate filter to only use the closest vendor outlet
     *
     * @return self
     */
    public function forceRetrieveOneClosestVendorOutlet()
    {
        $this->use_closest_outlet = true;

        return $this;
    }

    /**
     * Determines whether to force the use of only the closest vendor outlet
     *
     * @return bool
     */
    private function shouldUseClosestVendorOutlet()
    {
        return $this->use_closest_outlet
            || $this->request->query('use_closest_outlet', false) === 'true';
    }

    /**
     * Creates a query that represents a table containing records of all
     * vendor outlets ordered by the distance of the outlet to the provided coordinates
     *
     * Each record represents the closest outlet of the same vendor from the coordinates
     *
     * @param float $longitude
     * @param float $latitude
     *
     * @return \Illuminate\Database\Query\Builder;
     */
    private function closestVendorOutletsPerVendorFromCoordinates(
        float $longitude,
        float $latitude
    ) {
        return DB::query()->fromSub(
            function ($query) use ($longitude, $latitude) {
                $sub_query = queryOfTableWithDistanceInKmBasedOnCoordinates(
                    'vendor_outlets',
                    $longitude,
                    $latitude
                )
                    ->orderBy('distance');

                $columns = [
                    '*',
                    '(row_number() over (partition by vendor_id order by distance)) as rank_number'
                ];

                $query->selectRaw(implode(', ', $columns))
                    ->fromSub($sub_query, 'outlets_with_distance');
            },
            'distance_ranked_outlets'
        )
            ->where('rank_number', 1);
    }
}
