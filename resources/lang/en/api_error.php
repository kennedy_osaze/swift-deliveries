<?php

return [
    'not_found' => 'Resource not found.',

    'payment_unavailable_order' => 'No order is available for payment.',
    'order_not_payable' => 'This order is not available for payment.',
    'billing_contact_required' => 'A billing contact needs to be attached to this order.',
    'cart_already_used' => 'Cart cannot be processed as it has been checked out already.',
    'card_insufficient_balance' => 'Insufficient balance in selected card.',
    'card_not_reusable' => 'The selected payment card cannot be reused for charging the user',
    'invalid_transaction_reference' => 'The transaction reference is invalid.',
    'transaction_failed' => 'Transaction with the reference was not successful.',
    'incomplete_payment_amount' => 'The amount paid is less than the order total amount'
];
