<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Swift Deliveries | Order Foods &amp; Drinks</title>

        <!-- Styles -->
        <!-- <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>
        <header class="header">
            <nav class="top-nav">
                <img class="top-nav-logo" src="{{ asset('images/logo-purple.png') }}" alt="" />

                <ul class="top-nav-links">
                    <li class="top-nav-links__link signup-btn">
                        <a href="#" class="top-nav-links__link-item">Sign Up</a>
                    </li>
                    <li class="top-nav-links__link login-btn">
                        <a href="#" class="top-nav-links__link-item">Login</a>
                    </li>
                </ul>
            </nav>

            <div class="hero">
                <p class="hero__headline">
                    The best foods in Lagos straight to your doorstep.
                </p>

                <p class="hero__subheadline">
                    The best foods, drinks and groceries available for pickup and delivery near you.
                </p>

                <div class="download-links">
                    <a href="#"><img src="{{ asset('images/google-icon-download.png') }}" alt=""></a>
                    
                    <a href="#"><img src="{{ asset('images/apple-icon-download.png') }}" alt=""></a>
                </div>
            </div>
            
            <img src="{{ asset('images/hero__image-1.png') }}" alt="" class="hero__image hero__image--1">
            <img src="{{ asset('images/hero__image-2.png') }}" alt="" class="hero__image hero__image--2">
            <img src="{{ asset('images/hero__image-3.png') }}" alt="" class="hero__image hero__image--3">
            <img src="{{ asset('images/hero__image-4.png') }}" alt="" class="hero__image hero__image--4">
        </header>

        <section class="app-features">
            <div class="app-features__left">
                <div class="app-features__left-inner">
                    <p class="app-features__left-leadtext">Make your orders, track delivery and make payments, all within the app.</p>

                    <p class="app-features__left-subtext">Our sleek app has been perfectly designed to give you the best experience when making an order. You also get to track your order and know exactly where it is.</p>
                </div>
            </div>

            <div class="app-features__right">
                <img src="{{ asset('images/app-features-image.png') }}" alt="">
            </div>
        </section>

        <section class="food-categories">
            <div class="food-category food-category--1">
                <p class="food-category__name">Fast Foods</p>
                <img src="{{ asset('images/category__image-1.png') }}" alt="" class="food-category__image">
            </div>

            <div class="food-category food-category--2">
                <p class="food-category__name">African Cuisine</p>
                <img src="{{ asset('images/category__image-2.png') }}" alt="" class="food-category__image">
            </div>

            <div class="food-category food-category--3">
                <p class="food-category__name">Drinks</p>
                <img src="{{ asset('images/category__image-3.png') }}" alt="" class="food-category__image">
            </div>
            
            <div class="food-category food-category--4">
                <p class="food-category__name">Snacks</p>
                <img src="{{ asset('images/category__image-4.png') }}" alt="" class="food-category__image">
            </div>
        </section>

        <section class="app-features favorites">
            <div class="app-features__left">
                <div class="app-features__left-inner">
                    <p class="app-features__left-leadtext">Your favorite foods &amp; restaurants,<br> whenever you want it.</p>

                    <p class="app-features__left-subtext"> What's your idea of a perfect restaurant? We bring you the restaurant down to your doorstep without you having to move an inch. Nothing beats that.</p>

                    <div class="download-links">
                        <a href="#"><img src="{{ asset('images/google-icon-download.png') }}" alt=""></a>
                        
                        <a href="#"><img src="{{ asset('images/apple-icon-download.png') }}" alt=""></a>
                    </div>
                </div>
            </div>

            <div class="app-features__right">
                <img src="{{ asset('images/category__image-6.png') }}" alt="">
            </div>
        </section>

        <!-- <section class="comments">
            <div class="comments-inner">
                <p class="comments-inner__leadtext">We've partnered with your favorite restaurants around Yaba to bring you the very best.</p>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <p>Iyan Aladuke</p>
                        </div>
                        <div class="swiper-slide">
                            <p>Shop 10</p>
                        </div>
                        <div class="swiper-slide">
                            <p>Mavise</p>
                        </div>
                        <div class="swiper-slide">
                            <p>Iya Moriah</p>
                        </div>
                    </div>
                    
                    <div class="swiper-pagination"></div>
                </div>

            </div>
        </section> -->

        <section class="downloads">
            <div class="downloads-container">
                <p class="downloads-leadtext">Yet to download the app? Get it now.</p>
                
                <div class="download-links">
                    <a href="#"><img src="{{ asset('images/google-icon-download.png') }}" alt=""></a>
                    
                    <a href="#"><img src="{{ asset('images/apple-icon-download.png') }}" alt=""></a>
                </div>
                
                <p class="downloads-subtext">Or enter your email address below and we'll send you a link to download the app.</p>

                <div class="form-control">
                    <input type="text" placeholder="Enter your email address here">
                    <div class="form-control__icon">
                        &rarr;
                    </div>
                </div>
            </div>
        </section>

        <footer class="footer">
            <div class="footer__top">
                <div class="footer__logo-group">
                    <img class="footer__logo" src="{{ asset('images/logo-wb.png') }}" alt="" />
                    <p>Swift Deliveries</p>
                </div>

                <ul class="footer__others">
                    <li class="footer__others-item">
                        <a href="#" class="footer__others-link">
                            Complaints
                        </a>
                    </li>
                    <li class="footer__others-item">
                        <a href="#" class="footer__others-link">
                            Become a Rider
                        </a>
                    </li>
                </ul>

                <div class="download-links">
                    <a href="#"><img src="{{ asset('images/google-icon-download.png') }}" alt=""></a>
                    
                    <a href="#"><img src="{{ asset('images/apple-icon-download.png') }}" alt=""></a>
                </div>
            </div>

            <hr>

            <div class="footer__bottom">
                <p class="copyright">
                    &copy; 2019 All rights reserved.
                </p>

                <ul class="nav-bottom">
                    <!-- <li class="nav-bottom-item">
                        <a href="#" class="nav-bottom-link">
                            <i class="fab fa-facebook-f fa-2x"></i>
                        </a>
                    </li> -->
                    <li class="nav-bottom-item">
                        <a href="https://twitter.com/SwiftDlvrys" class="nav-bottom-link" target="_blank">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </li>
                    <li class="nav-bottom-item">
                        <a href="https://instagram.com/swiftdeliveriesng?igshid=vp67kwuezjzh" class="nav-bottom-link" target="_blank">
                            <i class="fab fa-instagram fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </footer>

        <!-- <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
        <script>
            var swiper = new Swiper('.swiper-container', {
                spaceBetween: 30,
                centeredSlides: true,
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        </script> -->
    </body>
</html>
