@extends('admin.layouts.main')
@section('content')
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                      <span class="h2 font-weight-bold mb-0">2,356</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 3.48%</span>
                    <span class="text-nowrap">Since last week</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                      <span class="h2 font-weight-bold mb-0">924</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 1.10%</span>
                    <span class="text-nowrap">Since yesterday</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                      <span class="h2 font-weight-bold mb-0">49,65%</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Product Items Tables</h3>
              <ul class="nav nav-pills nav-pills-circle float-right">
                <li class="nav-item">
                <a class="nav-link" id="contact-tab" href="{{route('vendor.product.create')}}">
                    <span class="nav-link-icon d-block"><i class="ni ni-fat-add"></i></span>
                </a>
                </li>
              </ul>
              @include('admin.layouts.alert')
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Vendor Name</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Outlet City</th>
                    <th scope="col">Status</th>
                    <th scope="col">Images</th>
                    <th scope="col">Activate</th>
                    <th scope="col">Created At</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($vendor_products as $vendor_product)
                        <tr>
                            <td>
                                {{$vendor_product->outlet->vendor->name}}
                            </td>
                            <td>
                                {{$vendor_product->product->name}}
                            </td>
                            <td>
                                {{$vendor_product->outlet->city}}
                            </td>
                            <td>
                              <span class="badge badge-dot mr-4">
                                  @if($vendor_product->active_at == null)
                                    <i class="bg-warning"></i> Not Active
                                  @else
                                    <i class="bg-success"></i> Active
                                  @endif
                              </span>
                            </td>
                            <td>
                              <div class="avatar-group">
                                @php
                                  $images = $vendor_product->outlet->vendor->productImages->take(5);
                                @endphp
                                @foreach($images as $image)
                                <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $vendor_product->product->name }}">
                                  @if($image->image != '/' )
                                      <img alt="" src="{{ asset($image->image) }}" class="rounded-circle">
                                  @else
                                      <img alt="" src="{{ asset('/products/default.jpeg') }}">
                                  @endif
                                  </a>
                                @endforeach
                              </div>
                            </td>
                            <td>
                            <div class="avatar-group" onclick="$(this).find('form').submit();">
                                <label class="custom-toggle">
                                  @if( $vendor_product->active_at == null)
                                    <input type="checkbox" >
                                  @else
                                    <input type="checkbox" checked>
                                  @endif
                                    <span class="custom-toggle-slider rounded-circle"></span>
                                </label>
                                <form action="{{ route('vendor.product.update', $vendor_product->id) }}" method="post">
                                  @csrf
                                  <input name="is_available" value="true" style="display: none">
                                </form>
                            </div>
                            </td>
                            <td>
                                {{$vendor_product->created_at}}
                            </td>
                            <td class="text-right">
                            <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="{{ route('vendor.product.edit', $vendor_product->id) }}">Edit</a>
                                <a class="dropdown-item" href="javascript:void(0);" onclick="$(this).find('form').submit();" >Delete
                                  <form action="{{ route('vendor.product.delete', $vendor_product->id) }}" method="post">
                                    @csrf
                                  </form>
                                </a>
                                </div>
                            </div>
                            </td>
                        </tr>
                    @endforeach 
                </tbody>
              </table>
            </div>
            {{ $vendor_products->links() }}
          </div>
        </div>
      </div>
    </div>
@endsection 