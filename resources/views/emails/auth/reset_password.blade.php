@extends('layouts.email')
​
@section('content')
​
<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">

    <tr style="border-collapse:collapse;">
        <td align="left" style="padding:0;Margin:0;padding-bottom:10px;">

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#333333;">
                Hello {{ $user->first_name }},
            </p>

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br>
            </p>

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#333333;">
                You are receiving this email because we received a password reset request for your account.<br>
                Please follow the link below to reset your password.
            </p>
        </td>
    </tr>

    <tr style="border-collapse:collapse;">
        <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:15px;padding-bottom:15px;">
            <span class="es-button-border" style="border-style:solid;border-color:#2CB543;background:#31CB4B;border-width:0px;display:inline-block;border-radius:3px;width:auto;">
                <a href="{{ $password_reset_link }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:16px;color:#FFFFFF;border-style:solid;border-color:#31CB4B;border-width:10px 15px;display:inline-block;background:#31CB4B;border-radius:3px;font-weight:normal;font-style:normal;line-height:19px;width:auto;text-align:center;">
                    Reset Password
                </a>
            </span>
        </td>
    </tr>

    <tr style="border-collapse:collapse;">
        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:20px;">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#333333;">
                Please note that this password reset link will expire in {{ config('auth.passwords.'.config('auth.defaults.passwords').'.expire') }} minutes.
            </p>

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br>
            </p>

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#333333;">
                If you did not request a password reset, no further action is required.
            </p>

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><br>
            </p>

            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#333333;">
                Thanks.
            </p>
        </td>
    </tr>
</table>
​
@endsection
