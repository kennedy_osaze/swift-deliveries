@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Email Verified') }}</div>

                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        {{ __('Your email has been verified successfully.') }}
                    </div>

                    {{ __('You can now proceed to log into the :app_name app to start placing orders.', ['app_name' => config('app.name')]) }}.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
