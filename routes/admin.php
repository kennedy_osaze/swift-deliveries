<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\Dispatch\TeamController;
use App\Http\Controllers\Admin\Vendors\VendorController;
use App\Http\Controllers\Admin\Products\ProductController;
use App\Http\Controllers\Admin\Dispatch\DispatchController;
use App\Http\Controllers\Admin\Vendors\OutletAdminController;
use App\Http\Controllers\Admin\Vendors\VendorOutletController;
use App\Http\Controllers\Admin\Vendors\VendorProductController;
use App\Http\Controllers\Admin\Products\ProductCategoryController;
use App\Http\Controllers\Admin\Vendors\Auth\OutletLoginController;

Auth::routes();

Route::get('outlet/login', [OutletLoginController::class, 'showLoginForm']);
Route::post('outlet/login', [OutletLoginController::class, 'login']);

Route::middleware(['auth:outlet_admin', 'is-outlet-admin'])->group(function () {
    Route::get('outlet/dashboard', [OutletAdminController::class, 'index']);
    Route::get('outlet/show/processed', [OutletAdminController::class, 'processedOrders']);
    Route::get('outlet/show/{id}', [OutletAdminController::class, 'show'])->name('outlet_admin.order.show');
    Route::post('outlet/edit/{id}', [OutletAdminController::class, 'update'])->name('outlet_admin.order.update');
});

Route::middleware(['auth:admin','is-admin'])->group(function () {
    Route::get('dashboard', [IndexController::class, 'index']);

    //All Products Categories Routes
    Route::prefix('product_categories')->group(function () {
        Route::get('index', [ProductCategoryController::class, 'index']);
        Route::get('create', [ProductCategoryController::class, 'create'])->name('product.category.create');
        Route::post('create', [ProductCategoryController::class, 'store']);
        Route::get('edit/{id}', [ProductCategoryController::class, 'edit'])->name('product.category.edit');
        Route::post('edit/{id}', [ProductCategoryController::class, 'update'])->name('product.category.update');
        Route::post('delete/{id}', [ProductCategoryController::class, 'destroy'])->name('product_categories.delete');
    });

    //All Products Routes
    Route::prefix('products')->group(function () {
        Route::get('index', [ProductController::class, 'index']);
        Route::get('create', [ProductController::class, 'create'])->name('product.create');
        Route::post('create', [ProductController::class, 'store']);
        Route::get('edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
        Route::post('edit/{id}', [ProductController::class, 'update'])->name('product.update');
        Route::post('delete/{id}', [ProductController::class, 'destroy'])->name('product.delete');
    });

    //All Vendor Routes
    Route::prefix('vendors')->group(function () {
        Route::get('index', [VendorController::class, 'index']);
        Route::get('create', [VendorController::class, 'create'])->name('vendor.create');
        Route::post('create', [VendorController::class, 'store']);
        Route::get('edit/{id}', [VendorController::class, 'edit'])->name('vendor.edit');
        Route::post('edit/{id}', [VendorController::class, 'update'])->name('vendor.update');
        Route::post('delete/{id}', [VendorController::class, 'destroy'])->name('vendor.delete');
    });

    //All Vendor Outlet Routes
    Route::prefix('vendor_outlets')->group(function () {
        Route::get('index', [VendorOutletController::class, 'index']);
        Route::get('create', [VendorOutletController::class, 'create'])->name('vendor.outlet.create');
        Route::post('create', [VendorOutletController::class, 'store']);
        Route::get('edit/{id}', [VendorOutletController::class, 'edit'])->name('vendor.outlet.edit');
        Route::post('edit/{id}', [VendorOutletController::class, 'update'])->name('vendor.outlet.update');
        Route::post('delete/{id}', [VendorOutletController::class, 'destroy'])->name('vendor.outlet.delete');
    });

    //All Product Items Routes
    Route::prefix('vendor_products')->group(function(){
        Route::get('index', [VendorProductController::class, 'index']);
        Route::get('create', [VendorProductController::class, 'create'])->name('vendor.product.create');
        Route::post('create', [VendorProductController::class, 'store']);
        Route::get('edit/{id}', [VendorProductController::class, 'edit'])->name('vendor.product.edit');
        Route::post('edit/{id}', [VendorProductController::class, 'update'])->name('vendor.product.update');
        Route::post('delete/{id}', [VendorProductController::class, 'destroy'])->name('vendor.product.delete');
    });

    //All Dispatch Routes
    Route::prefix('dispatchs')->group(function(){
        Route::get('index', [DispatchController::class, 'index']);
        Route::get('create', [DispatchController::class, 'create'])->name('dispatch.create');
        Route::post('create', [DispatchController::class, 'store']);
        Route::get('edit/{id}', [DispatchController::class, 'edit'])->name('dispatch.edit');
        Route::post('edit/{id}', [DispatchController::class, 'update'])->name('dispatch.update');
        Route::post('delete/{id}', [DispatchController::class, 'destroy'])->name('dispatch.delete');
    });

    //All Dispatch Teams Routes
    Route::prefix('dispatch_teams')->group(function(){
        Route::get('index', [TeamController::class, 'index']);
        Route::get('create', [TeamController::class, 'create'])->name('dispatch.teams.create');
        Route::post('create', [TeamController::class, 'store']);
        Route::get('edit/{id}', [TeamController::class, 'edit'])->name('dispatch.teams.edit');
        Route::post('edit/{id}', [TeamController::class, 'update'])->name('dispatch.teams.update');
        Route::post('delete/{id}', [TeamController::class, 'destroy'])->name('dispatch.teams.delete');
    });

});


