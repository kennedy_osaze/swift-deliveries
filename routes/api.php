<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->name('api.')->group(function () {
    /** Authentication Routes */
    Route::post('auth/register', 'Auth\RegisterController@register')
        ->name('auth.register');
    Route::post('auth/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
        ->name('auth.password.email');
    Route::post('auth/login', 'Auth\LoginController@login')
        ->name('auth.login');
    Route::post('auth/logout', 'Auth\LoginController@logout')
        ->name('auth.logout');

    // Authenticated Routes
    Route::middleware('auth:api')->group(function () {
        /** Product Categories Routes */
        Route::get('product-categories', 'Product\CategoryController@index')
            ->name('product.category.index');
        Route::get('product-categories/{category}', 'Product\CategoryController@show')
            ->name('product.category.show');
        Route::get('product-categories/{category}/vendors', 'Product\CategoryController@showVendors')
            ->name('product.category.vendor.show');
        Route::get('product-categories/{category}/products', 'Product\CategoryController@showProducts')
            ->name('product.category.product.show');
        Route::get('product-categories/{category}/product-items', 'Product\CategoryController@showProductItems')
            ->name('product.category.item.show');

        /** Vendors Routes */
        Route::get('vendors', 'Vendor\VendorController@index')
            ->name('vendor.index');
        Route::get('vendors/{vendor}', 'Vendor\VendorController@show')
            ->name('vendor.show');
        Route::get('vendors/{vendor}/products', 'Vendor\VendorController@showProducts')
            ->name('vendor.product.show');
        Route::get('vendors/{vendor}/product-items', 'Vendor\VendorController@showProductItems')
            ->name('vendor.item.show');
        Route::get('vendors/{vendor}/product-categories', 'Vendor\VendorController@showProductCategories')
            ->name('vendor.product.category.show');

        /** Products Routes */
        Route::get('products', 'Product\ProductController@index')
            ->name('products.index');
        Route::get('products/{product}', 'Product\ProductController@show')
            ->name('product.show');
        Route::get('products/{product}/product-items', 'Product\ProductController@showProductItems')
            ->name('product.items.show');
        Route::get('products/{product}/vendors', 'Product\ProductController@showVendors')
            ->name('product.vendors.show');

        /** Product Items Routes */
        Route::get('product-items', 'Product\ItemController@index')
            ->name('product.item.index');

        /** Search and Filter Routes */
        Route::get('product-items/search', 'Product\ItemSearchController@search')
            ->name('product.item.search');

        /** Get Favoured Product Items */
        Route::get('product-items/favourites', 'User\FavouriteController@showProductItems')
            ->name('product.item.favourites.index');

        Route::get('product-items/{item}', 'Product\ItemController@show')
            ->name('product.item.show');

        /** Favourites (Product Items) Routes */
        Route::post('product-items/{item}/favourites', 'User\FavouriteController@store')
            ->name('product.item.favourites.store');
        Route::delete('product-items/{item}/favourites', 'User\FavouriteController@destroy')
            ->name('product.item.favourites.destroy');

        /** User Cart Routes */
        Route::get('cart', 'User\CartController@index')
            ->name('cart.index');
        Route::post('cart', 'User\CartController@store')
            ->name('cart.store');
        Route::post('cart/items', 'User\CartController@storeMultiple')
            ->name('cart.store.multiple');
        Route::delete('cart', 'User\CartController@destroy')
            ->name('cart.destroy');
        Route::get('cart/{item}', 'User\CartController@showItem')
            ->name('cart.item.show');
        Route::put('cart/{item}', 'User\CartController@updateItem')
            ->name('cart.item.update');
        Route::delete('cart/{item}', 'User\CartController@deleteItem')
            ->name('cart.item.destroy');

        /** Checkout Route */
        Route::post('checkout', 'User\CheckoutController@checkout')
            ->name('checkout');

        /** Billing Contact Routes */
        Route::get('billing-contacts', 'User\BillingContactController@index')
            ->name('billing-contact.index');
        Route::post('billing-contacts', 'User\BillingContactController@store')
            ->name('billing-contact.store');
        Route::get('billing-contacts/{contact}', 'User\BillingContactController@show')
            ->name('billing-contact.show');
        Route::put('billing-contacts/{contact}', 'User\BillingContactController@update')
            ->name('billing-contact.update');
        Route::delete('billing-contacts/{contact}', 'User\BillingContactController@destroy')
            ->name('billing-contact.destroy');

        /** Payment Card Controller */
        Route::delete('payment-card/{card}', 'User\PaymentCardController@destroy')
            ->name('payment-card.destroy');

        /** Card Payment Routes */
        Route::post('payment/card/initiate/{order?}', 'Payment\CardController@processOrderWithNewCard')
            ->name('payment.card.initiate');
        Route::post('payment/card/pay/{order?}', 'Payment\CardController@payOrderWithSavedCard')
            ->name('payment.card.pay');
        Route::post('payment/card/complete-transaction/{reference}', 'Payment\CardController@completeTransaction')
            ->name('payment.card.complete');

        /** Order Routes */
        Route::get('orders', 'User\OrderController@index')
            ->name('orders.index');
        Route::get('orders/{order}', 'User\OrderController@show')
            ->name('orders.show');
        Route::post('orders/{order}/cancel', 'User\OrderController@cancel')
            ->name('orders.cancel');
    });

    /** Webhook Routes */
    Route::post('payment/callback', 'Payment\WebhookController@handle')
        ->name('payment.callback');
});
