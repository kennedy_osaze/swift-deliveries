<?php

return [
    'order' => [

        // The size of a unique reference code for an order
        'reference_length' => env('REFERENCE_LENGTH', 15),
    ]
];
